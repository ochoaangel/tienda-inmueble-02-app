import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { MyseviceService } from './services/mysevice.service';
import { VariablesService } from './services/variables.service';

import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  btniniciar = true;
  email = 'Usuario';

  ngOnInit() {
    console.log('ngOnInit desde app.components solo la primera vez...');

    this.mys.getInmuebles().subscribe(data => {
      // caso cuando si hay conexión o transmisioón de datos

      if (data.status) {
        // guardo los inmuebles en mys

        // ordeno los inmuebles de mayor a menor visitas
        const ordenadoXvisitas = _.sortBy(data.payload, 'cont_visitas');
        this.mys.inmueblesTodos = ordenadoXvisitas.reverse();
        console.log('this.mys.inmueblesTodos', this.mys.inmueblesTodos);

        // verifico si es primera vez que se abre la app luego de ser instalada
        this.mys.checkStorage('TiendaInmueble1raVez').subscribe(data => {
          if (data) {
            // caso que ya inició luego de ser instalada y ya mostró la presentacion
            this.mys.IniciadaApp = true;
          } else {
            // caso que ya inició luego de ser instalada y no se HA mostrado la presentacion
            this.mys.IniciadaApp = false;
          }
        });



        // verifico si es primera vez que se hace una visita
        this.mys.checkStorage('TiendaInmueble1raVisita').subscribe(data => {
          if (data) {
            // caso que ya se hizo una visita y no es necesario mostrar slider de visita
            this.mys.IniciadaVisita = true;
          } else {
            // caso que no se ha hecho una visita y ES necesario mostrar slider de visita
            this.mys.IniciadaVisita = false;
          }
        });
      } else {
        // caso No hay Conexión a Internet o hubo un error en la comunicación
        console.log(
          'xXXXXxxxxxxxxx No hay Conexión a Internet o hubo un error en la comunicación xXXXXxxxxxxxxx'
        );
      }
    });


  }




  // tslint:disable-next-line: member-ordering
  public appPages = [
    {
      title: 'HOME',
      url: '/home',
      icon: 'images'
    },
    {
      title: 'Favoritos',
      url: '/favorites',
      icon: 'heart'
    },
    {
      title: 'Visitas',
      url: '/view-open',
      icon: 'copy'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public httpClient: HttpClient,
    private st: Storage,
    private mys: MyseviceService, // private http: HttpClient
    private myv: VariablesService, // private http: HttpClient
    private menuController: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    // this.mys.loadingPresent();
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
