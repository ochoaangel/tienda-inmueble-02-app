import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, PRIMARY_OUTLET } from '@angular/router';
import { AlertController, ActionSheetController, LoadingController } from '@ionic/angular';
import { MyseviceService } from '../services/mysevice.service';
import { MenuController } from '@ionic/angular';
import { VariablesService } from '../services/variables.service';
import { Platform } from '@ionic/angular';
import { Location } from '@angular/common';
import { present } from '@ionic/core/dist/types/utils/overlays';
import { HttpClient } from '@angular/common/http';

import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  filtros = { f1: '', f2: '', f3: '', f4: '', f5: '' };
  nfiltro = 0;

  inmueblesTodos = [];

  constructor(
    private router: Router,
    private alertController: AlertController,
    private mys: MyseviceService, // private http: HttpClient
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public location: Location,
    public menuController: MenuController,
    public httpClient: HttpClient
  ) {
    this.filtros = { f1: '', f2: '', f3: '', f4: '', f5: '' };
  }

  public unsubscribeBackEvent: any; // variable para boton back

  alertSalir = false;
  actionSheetVisible = false;
  actionSheet;

  urlBase = '';
  urlBaseImagenes2d = '';
  urlApi = '';



  loading: any;
  user: any;
  userSigned = false;
  email: string;
  inmuebles: any;

  ngOnInit() {
    console.log('ngOnInit desde home solo la primera vez...');
    this.initializeBackButtonCustomHandler(); // para capturar boton back
  }

  setFiltro0() {
    this.filtros.f1 = '';
    this.filtros.f2 = '';
    this.filtros.f3 = '';
    this.filtros.f4 = '';
    this.filtros.f5 = '';
    this.nfiltro = 0;
  }
  setFiltro1(valor: string) {
    this.filtros.f1 = valor;
    this.filtros.f2 = '';
    this.filtros.f3 = '';
    this.filtros.f4 = '';
    this.filtros.f5 = '';
    this.nfiltro = 1;
  }
  setFiltro2(valor: string) {
    this.filtros.f2 = valor;
    this.filtros.f3 = '';
    this.filtros.f4 = '';
    this.filtros.f5 = '';
    this.nfiltro = 2;
  }
  setFiltro3(valor: string) {
    this.filtros.f3 = valor;
    this.filtros.f4 = '';
    this.filtros.f5 = '';
    this.nfiltro = 3;
  }
  setFiltro4(valor: string) {
    this.filtros.f4 = valor;
    this.filtros.f5 = '';
    this.nfiltro = 4;
  }
  setFiltro5(valor: string) {
    this.filtros.f5 = valor;
    this.nfiltro = 5;
    console.log('Termino los filtros..', this.filtros);
    // this.setFiltro0();
    // console.log('Termino los filtros..', this.filtros);
  }

  regresarFiltro() {
    switch (this.nfiltro) {
      case 1:
        this.setFiltro0();
        break;
      case 2:
        this.filtros.f2 = '';
        this.filtros.f3 = '';
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 1;
        break;
      case 3:
        this.filtros.f3 = '';
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 2;
        break;
      case 4:
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 3;
        break;
      case 5:
        this.filtros.f5 = '';
        this.nfiltro = 4;
        break;

      default:
        this.setFiltro0();
        break;
    }
  }

  ionViewDidEnter() {
    this.inmueblesTodos = this.mys.inmueblesTodos;
    this.urlBase = this.mys.urlBase;
    this.urlBaseImagenes2d = this.mys.urlBaseImagenes2d;
    this.urlApi = this.mys.urlApi;
    console.log('this.inmueblesTodos dsd DidEnter', this.inmueblesTodos);
  }

  detalleinmueble(ninmueble: number) {
    console.log(ninmueble);
    this.mys.houseDetailId = ninmueble;
    this.router.navigateByUrl('house-detail');
  }

  // ionViewDidEnter() {
  //   if (this.mys.isLoading) {
  //     this.mys.loadingDismiss();
  //   }
  // }

  async alanzarMenu() {
    this.actionSheet = await this.actionSheetCtrl.create({
      backdropDismiss: true,
      buttons: [
        {
          text: 'Compartir en Redes',
          icon: 'share',
          cssClass: 'actionSheet-Global-inmueble',
          handler: () => {
            console.log('Se pulsó en Compartir');
          }
        },
        {
          text: 'Añadir a Favorito',
          icon: 'star',
          cssClass: 'actionSheet-Global-inmueble',
          handler: () => {
            console.log('Se pulsó en Añadir a Favorito');
          }
        }
      ]
    });
    this.actionSheetVisible = true;
    this.actionSheet.present();
  }

  initializeBackButtonCustomHandler(): void {
    this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(
      999999,
      () => {
        this.menuController.isOpen().then(ret => {
          if (ret) {
            // si esta abierto el menu lo cierro
            this.menuController.close('mymenu');
          } else {
            // si NO esta abierto el menu
            if (this.actionSheetVisible) {
              this.actionSheetVisible = false;
              this.actionSheet.dismiss();
            } else {
              if (this.router.url === '/home') {
                if (this.nfiltro === 0) {
                  if (!this.alertSalir) {
                    this.alertSalir = !this.alertSalir;
                    this.presentAlertConfirm();
                  }
                } else {
                  switch (this.nfiltro) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                      this.regresarFiltro();
                      break;
                    default:
                      break;
                  }
                }
              } else {
                this.location.back();
              }
            }
          }
        });
      }
    );
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Desea Cerrar la Aplicación?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: blah => {
            console.log('Volviendo a la App  ' + this.constructor.name);
            this.alertSalir = !this.alertSalir;
          }
        },
        {
          text: 'SI',
          handler: () => {
            console.log('Cerrando la App');
            this.alertSalir = !this.alertSalir;
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  OnDestroy() {
    this.unsubscribeBackEvent && this.unsubscribeBackEvent();
    console.log('fffffffffffffffffffhhhhhhhhhhhhhhhhhh');
  }
}
