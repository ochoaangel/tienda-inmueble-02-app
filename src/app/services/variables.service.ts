import { Injectable } from '@angular/core';
import { User, Cita, Inmueble } from '../models/misInterfaces-interface';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VariablesService {
  // variables estaticas
  UrlBaseGeneral = 'http://192.168.16.106:8081/';
  VarGlobal = 'mydata'; // en  el index.html

  // variables ///////////////////
  urlinmuebles = 'http://localhost:8081/user?id=3';

  usuario = { email: '', password: '' };
  // url='http://localhost:8081/confirm?user=user03&pass=pass03';
  urlConfirm = 'confirm?user=';
  urlPass = '&pass=';


  constructor(private httpClient: HttpClient) { }

  ////////////////////////////////////////////////////////////////////////////////
  ///////////////////////// Variables Globales ///////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // public inmuebles = [];
  // public inmueble = {};
  // public user = {};
  // public citas = [];
  // public cita = {};

  // public inmuebles: Array<Inmueble>;
  // public inmueble: Inmueble;
  // public user: User;
  // public citas: Array<Cita>;
  // public cita: Cita;
  //  inmuebles: any;
  // public inmueble: any;
  // public user: any;
  // public citas: Array<any>;
  // public cita: any;

  ////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////// Funciones ////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // public getInmuebles(): Array<Inmueble> { return this.inmuebles; }
  // public setInmuebles(inmueblesX: Array<Inmueble>): void { this.inmuebles = inmueblesX; }
  // public isSetInmuebles(): boolean {
  //   console.log(this.inmuebles);
  //   console.log(this.inmuebles);
  //   if (this.inmuebles.length > 0) {
  //     return true;
  //   } else {
  //     return false;
  //   }

  // }

  // public getInmueble(): Inmueble { return this.inmueble; }
  // public setInmueble(inmuebleX: Inmueble): void { this.inmueble = inmuebleX; }
  // public isSetInmueble(): boolean {
  //   if (this.inmueble.hasOwnProperty('id')) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // public getUserSigned(): User { return this.user; }
  // public setUserSigned(userX: User): void { this.user = userX; }
  // public isSetUserSigned(): boolean {
  //   if (this.inmueble.hasOwnProperty('id')) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // public getCitas(): Array<Cita> { return this.citas; }
  // public setCitas(citasX: Array<Cita>): void { this.citas = citasX; }
  // public isSetCitas(): boolean {
  //   if (this.citas.length > 0) {
  //     return true;
  //   } else {
  //     return false;
  //   }

  // }

  // public getCita(): Cita { return this.cita; }
  // public setCita(citaX: Cita): void { this.cita = citaX; }
  // public isSetCita(): boolean {
  //   if (this.inmueble.hasOwnProperty('id')) {
  //     return true;
  //   } else {
  //     return false;
  //   }

  // }

  // public getAndSetInmuebles(): void {
  //   const urlFinal = this.UrlBaseGeneral + 'inmuebles';
  //   console.log('Getting la lista de inmuebles desde: ' + urlFinal);
  //   this.httpClient.get(urlFinal).subscribe(data => {

  //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy1');
  //     console.log(data);
  //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy2');
  //     // console.log(data);
  //     // console.log(data);
  //     // this.myv.setInmuebles(data);
  //     this.inmueble = data
  //     console.log(this.inmueble);
  //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy3');
  //     console.log(this.inmueble);
  //     console.log(this.inmueble);

  //   });
  // }


  ////////////////////////////////////////////////////////////////////////////////

  // public getGlobal(): any {
  //   return window[this.VarGlobal];
  // }

  // public isUserSigned(): boolean {
  //   const mydata = window[this.VarGlobal];
  //   console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhh1');
  //   console.log(mydata);
  //   console.log(mydata.inmuebles);
  //   console.log(mydata.user);
  //   console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhh2');

  //   if (mydata.user) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // public removeUserSigned(): void {
  //   let mydata = window[this.VarGlobal];
  //   delete mydata.user;
  //   window[this.VarGlobal] = mydata;
  // }

  // public setUserSigned(NewUser: any): void {
  //   let mydata = window[this.VarGlobal];
  //   mydata.user = NewUser;
  //   window[this.VarGlobal] = mydata;
  // }

  // public getUserSigned(): any {
  //   const mydata = window[this.VarGlobal];
  //   return mydata.user;
  // }

  // public isSetUserSigned(): boolean {
  //   const mydata = window[this.VarGlobal];
  //   if (mydata.user) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // public getInmuebleDetailId(ninmueble: number): any {
  //   const mydata = window[this.VarGlobal];
  //   const misInmuebles = mydata.inmuebles;
  //   const result = misInmuebles.filter(cinmueble => cinmueble.id === ninmueble);
  //   return result[0];
  // }

  // public setInmuebles(inmueblesNew: any): void {
  //   let mydata = window[this.VarGlobal];
  //   mydata.inmuebles = inmueblesNew;
  //   window[this.VarGlobal] = mydata;
  // }

  // public setInmueble(inmuebleNew: any): void {
  //   let mydata = window[this.VarGlobal];
  //   mydata.inmueble = inmuebleNew;
  //   window[this.VarGlobal] = mydata;
  // }

  // public getInmuebles(): any {
  //   const mydata = window[this.VarGlobal];
  //   return mydata.inmuebles;
  // }

  // public getInmueble(): any {
  //   const mydata = window[this.VarGlobal];
  //   const mydata2 = mydata.inmueble;
  //   return mydata2;
  // }

  // public isSetInmueble(): boolean {
  //   const mydata = window[this.VarGlobal];
  //   if (mydata.inmueble) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // public setRemoveInmueble(): void {
  //   let mydata = window[this.VarGlobal];
  //   delete mydata.inmueble;
  // }


  // public 


}
