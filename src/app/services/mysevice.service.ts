import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { VariablesService } from '../services/variables.service';
import { User, Cita, Inmueble } from '../models/misInterfaces-interface';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyseviceService {
  constructor(
    private httpClient: HttpClient,
    private st: Storage,
    // private myv: VariablesService,
    public loadingCtrl: LoadingController,
    public toastcontroller: ToastController
  ) { }

  // Variables URL Generales
  urlBase = 'http://www.tiendainmueble.com';
  urlBaseImagenes2d = this.urlBase + '/imgs/propiedades/';
  urlApi = this.urlBase + '/api/v1.0';

  // Variables para cuando inicie la aplicacion

  // Variables para cuando inicie la aplicacion X PRIMERA VEZ

  // variables para abrir paginas con parametros
  view360url = '';        // vista 360 del inmueble
  houseDetailId = 0;      // vista detalles del inmueble

  // Variables para guardar en el Programa
  inmueblesTodos = [];
  IniciadaApp = false;
  IniciadaVisita = false;

  // Variables para guardar en el teléfono (Storage)
  TiendaInmueble1raVez = false;
  TiendaInmueble1raVisita = false;

  public isLoading = false;
  // variables ///////////////////
  // http://www.tiendainmueble.com/api/v1.0
  // ejemplo imagen http://www.tiendainmueble.com/imgs/propiedades/157298472234.099-22-Bathroom%20(1).jpg
  // UrlBase = 'http://192.168.16.106:8081';
  // UrlBase = 'http://192.168.16.113:8081';
  // UrlBase = 'http://www.tiendainmueble.com';
  // VarGlobal = 'mydata'; // en  el index
  usuario = { email: '', password: '' };

  async loadingPresent() {
    this.isLoading = true;
    return await this.loadingCtrl
      .create({
        message: 'Cargando ...',
        spinner: 'circles'
      })
      .then(a => {
        a.present().then(() => {
          console.log('loading presented');
          if (!this.isLoading) {
            a.dismiss().then(() => console.log('abort laoding'));
          }
        });
      });
  }

  async loadingDismiss() {
    this.isLoading = false;
    return await this.loadingCtrl
      .dismiss()
      .then(() => console.log('loading dismissed'));
  }

  // ////////////////////////////////////////////////////////////////////
  // //////////////////////////// API ///////////////////////////////////
  // ////////////////////////////////////////////////////////////////////
  // public getinmuebles(): void {
  //   // guardo los inmuebles en storage
  //   this.httpClient
  //     .get(this.UrlBase + '/inmuebles')
  //     .subscribe(data => {
  //       this.st.set('inmuebles', data);
  //       console.log('Guardado inmuebles desde Services ' + data);
  //     });
  // }

  // ////////////////////////////////////////////////////////////////////
  // public usersigned(): any {
  //   this.st.get('user').then(valor => {
  //     if (valor) {
  //       console.log('Usuario registrado - desde service');
  //       return true;
  //     } else {
  //       console.log('Usuario NO registrado - desde service');
  //       return false;
  //     }
  //   });
  // }

  // public getemail(): any {
  //   this.st.get('user').then(valor => {
  //     if (valor) {
  //       return valor[0].user;
  //     } else {
  //       console.log(valor);
  //       return 'no hay acceso';
  //     }
  //   }); 
  // }

  // public getrol(): any {
  //   this.st.get('user').then(valor => {
  //     if (valor) {
  //       return valor[0].rol;
  //     } else {
  //       // console.log(valor);
  //       return 'no hay acceso';
  //     }
  //   });
  // }

  // ////////////////////////////////////////////////////////////////////
  // //////////////////////////// GENERAL ///////////////////////////////
  // public getAndSetInmuebles(): void {
  //   // let urlFinal = this.UrlBase + '/inmuebles';
  //   let urlFinal = this.UrlBase + '/api/v1.0/inmuebles';
  //   console.log('Getting la lista de inmuebles desde: ' + urlFinal);
  //   this.httpClient.get(urlFinal).subscribe(data => {
  //     console.log('-----------------------------------------------------------');
  //     console.log('data---->', data);
  //     this.myv.setInmuebles(data);
  //     // this.myv.setInmuebles(data)

  //   });
  // }

  // public confirmUser(user: any): void {
  //   let urlFinal =
  //     this.UrlBase +
  //     this.urlConfirm +
  //     user.email +
  //     this.urlPass +
  //     user.password;
  //   console.log('Confirmando usuario con : ' + urlFinal);

  //   this.httpClient.get(urlFinal).subscribe(data => {
  //     try {
  //       // caso si el usuario SI se Logeo
  //       if (data[0].phone) {
  //         this.myv.setUserSigned(data[0]);
  //         console.log('El usuario se logeo satisfactoriamente desde myService.');
  //       }
  //     } catch (error) {
  //       // caso si el usuario NO se Logeo
  //       console.log('El usuario NO se logeo desde myService.');
  //     }
  //   });
  // }

  ////////////////////////////////////////////////////////////////////////
  async toast_mostrar(message: string) {
    const toast = await this.toastcontroller.create({
      message,
      animated: true,
      color: 'primary',
      duration: 2000
    });
    toast.present();
  }
  ////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  // Sesiones
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  getInmuebles(): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      this.httpClient.get(this.urlApi + '/inmuebles').subscribe(data => {
        observer.next(data);
        observer.complete();
      });
    });
  }

  checkStorage(key: string): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>) => {
      this.st.get(key).then(data => {
        if (data) {
          observer.next(true);
          observer.complete();
        } else {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }

  setStorage(key: string, value: any): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>) => {
      this.st.set(key, value);
      this.st.get(key).then(data => {
        if (data) {
          observer.next(true);
          observer.complete();
        } else {
          observer.next(false);
          observer.complete();
        }
      });
    });
  }


  getStorage(key: string): Observable<any> {
    return new Observable((observer: Subscriber<any>) => {
      this.st.get(key).then(data => {
        if (data) {
          observer.next(data);
          observer.complete();
        } else {
          observer.next(undefined);
          observer.complete();
        }
      });
    });
  }


  // getInmuebles(): Observable<booleanOany> {
  //   return new Observable((observer: Subscriber<booleanOany>) => {
  //     observer.next(trueOvariable);
  //     observer.complete();
  //   });
  // } // fin gps_preguntarPrenderGps

  //para usar => this.getInmuebles().subscribe()
} /////////// fin my service
