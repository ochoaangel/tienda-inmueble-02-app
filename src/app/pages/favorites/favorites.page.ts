import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VariablesService } from '../../services/variables.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {

  constructor(private router: Router, private myv: VariablesService) { }

  ngOnInit() { }

  onClick(ninmueble: number) {
    // let inmuebles = this.myv.getInmuebleDetailId(ninmueble);
    // this.myv.setInmueble(inmuebles);
    // this.router.navigateByUrl('house-detail');
  }

}
