import { Component, OnInit } from '@angular/core';
import { VariablesService } from '../../services/variables.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  albumes: any[] = [];
  textoBuscar = '';

  constructor(
    // public myv: VariablesService,
     public router: Router) { }

  ngOnInit() {
    this.albumes = [
      { 'title': 'Casa1 ' },
      { 'title': 'Casa2 ' },
      { 'title': 'Casa3 ' },
      { 'title': 'Apt1 ' },
      { 'title': 'Apt2 ' },
      { 'title': 'Apt3 ' },
      { 'title': 'Terreno1 ' },
      { 'title': 'Terreno2 ' },
      { 'title': 'Terreno3 ' },
      { 'title': 'Apartamento1 ' },
      { 'title': 'Apartamento2 ' },
      { 'title': 'Apartamento3 ' },
      { 'title': 'Casa1 con patio ' },
      { 'title': 'Casa2 con patio ' },
      { 'title': 'Casa3 con patio ' },
      { 'title': 'Apt1 en avenida ' },
      { 'title': 'Apt2 en avenida ' },
      { 'title': 'Apt3 en avenida ' },
      { 'title': 'Terreno1 con bosque ' },
      { 'title': 'Terreno2 con bosque ' },
      { 'title': 'Terreno3 con bosque ' },
      { 'title': 'Apartamento1 para familia ' },
      { 'title': 'Apartamento2 para familia ' },
      { 'title': 'Apartamento3 para familia ' }
    ]
  }

  buscar(event) {
    this.textoBuscar = event.detail.value;
  }

  onClickItem(ninmueble: number) {
    // let inmuebles = this.myv.getInmuebleDetailId(ninmueble);
    // this.myv.setInmueble(inmuebles);
    this.router.navigateByUrl('house-detail');

  }
}


