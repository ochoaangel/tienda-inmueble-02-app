import { Component, OnInit } from '@angular/core';
// nuevos
import { Router } from '@angular/router';
import { AlertController, IonSlides, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyseviceService } from '../../services/mysevice.service';
import { VariablesService } from '../../services/variables.service';

import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.page.html',
  styleUrls: ['./house-detail.page.scss']
})
export class HouseDetailPage implements OnInit {
  rol: string;
  user: string;
  inmueble: any;
  public prueba = 'varhbthathasrtjjts';
  public titulox = 'mititulo';
  // public urlimage = this.mys.UrlBase + '/Images/Inmuebles/';
  // public urlimage = this.mys.UrlBase + '/imgs/propiedades/';

  // variables importantes para definir
  urlBase = '';
  urlBaseImagenes2d = '';
  urlApi = '';
  inmuebleAmostrar = {};
  

  // configuracion para los sliders
  options2d = { slidesPerView: 1 };
  options360 = { slidesPerView: 3.5 };

  array360url = [
    '../../../assets/360/1.jpg',
    '../../../assets/360/2.jpg',
    '../../../assets/360/3.jpg',
    '../../../assets/360/4.jpg',
    '../../../assets/360/5.jpg'
  ];

  constructor(
    private router: Router,
    private mys: MyseviceService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    // defino variables fundamentales
    this.urlBase = this.mys.urlBase;
    this.urlBaseImagenes2d = this.mys.urlBaseImagenes2d;
    this.urlApi = this.mys.urlApi;

    // seleccionando el inmueble de la lista (el que se va a mostrar)
    this.inmuebleAmostrar = _.findWhere(this.mys.inmueblesTodos, { id: this.mys.houseDetailId });
    console.log('Show', this.inmuebleAmostrar);



    // definir cuantas slides colocar en 360
    switch (this.array360url.length) {
      case 0:
        this.options360.slidesPerView = 0;
        break;
      case 1:
        this.options360.slidesPerView = 1;
        break;
      case 2:
        this.options360.slidesPerView = 2;
        break;
      case 3:
        this.options360.slidesPerView = 3;
        break;
      default:
        this.options360.slidesPerView = 3.5;
        break;
    }

  }

  // @ViewChild('') slides: IonSlides;

  planificarvisita() {
    // if (this.myv.isUserSigned()) {
    //   this.router.navigateByUrl('create-visit');
    // } else {
    //   this.router.navigateByUrl('login');
    // }

    // this.st.get('user').then(valor => {
    //   if (valor) {
    //     this.router.navigateByUrl('create-visit');
    //   } else {
    //     this.router.navigateByUrl('login');
    //   }
    // });
  } // fin planificarvisita

  ionViewWillEnter() { }

  ionViewDidEnter() { }

  ionViewDidLeave() {
    // this.myv.setRemoveInmueble();
  }

  whatsapp() {
    window.open('whatsapp://send?phone=584242332373?text=Me%20gustaría%20saber%20el%20precio%20del%20coche');
  }

  tourVirtual() {
    console.log('Éste inmueble tiene tour Virtual');
    this.view360url('../../../assets/360/a.jpg');
  }

  view360url(url: string) {
    this.presentLoading();
    this.mys.view360url = url;
    this.router.navigateByUrl('/view360');

  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Cargando Imagen 360°',
      duration: 500
    });
    await loading.present();
  }

} // fin clase
