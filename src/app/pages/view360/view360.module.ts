import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { View360Page } from './view360.page';

const routes: Routes = [
  {
    path: '',
    component: View360Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [View360Page],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class View360PageModule {}
