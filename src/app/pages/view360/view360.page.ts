import { Component, OnInit } from '@angular/core';
import { MyseviceService } from '../../services/mysevice.service';

@Component({
  selector: 'app-view360',
  templateUrl: './view360.page.html',
  styleUrls: ['./view360.page.scss'],
})
export class View360Page implements OnInit {

  constructor(private mys: MyseviceService) { }

  view360url;
  ngOnInit() {
    this.view360url = this.mys.view360url;
    console.log(this.mys.view360url);
  }

}
