import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
// nuevos
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-create-visit',
  templateUrl: './create-visit.page.html',
  styleUrls: ['./create-visit.page.scss']
})
export class CreateVisitPage implements OnInit {
  // tslint:disable:variable-name

  secuencia = 1;

  incompleto = true;
  id: number;                 // id de la cita 

  id_interesado: number;      // el que planifico la cita
  id_propietario: number;     // el dueno del inmueble
  id_corredor: number;        // el que aprobo la publicacion del inmueble

  // dia 1 //////////////////////////////////////////////////////////////////
  day_1: string;
  time_1_a_i: string;         // hora que planifico el interesado
  time_1_b_i: string;
  time_1_c_i: string;

  time_1_a_c: boolean;        // hora aprobada o NO por el Corredor
  time_1_b_c: boolean;
  time_1_c_c: boolean;

  time_1_a_p: boolean;        // hora aprobada o NO por el Propietario
  time_1_b_p: boolean;
  time_1_c_p: boolean;

  // dia 2 //////////////////////////////////////////////////////////////////
  day_2: string;
  time_2_a_i: string;
  time_2_b_i: string;
  time_2_c_i: string;

  time_2_a_c: boolean;
  time_2_b_c: boolean;
  time_2_c_c: boolean;

  time_2_a_p: boolean;
  time_2_b_p: boolean;
  time_2_c_p: boolean;

  // dia 3 //////////////////////////////////////////////////////////////////
  day_3: string;
  time_3_a_i: string;
  time_3_b_i: string;
  time_3_c_i: string;

  time_3_a_c: boolean;
  time_3_b_c: boolean;
  time_3_c_c: boolean;

  time_3_a_p: boolean;
  time_3_b_p: boolean;
  time_3_c_p: boolean;

  minDay1: any;
  minDay2: any;
  minDay3: any;

  maxDay1: any;
  maxDay2: any;
  maxDay3: any;



  statusDay1 = true;
  statusDay2 = false;
  statusDay3 = false;

  statusTime_1_a_i = false;
  statusTime_1_b_i = false;
  statusTime_1_c_i = false;

  statusTime_2_a_i = false;
  statusTime_2_b_i = false;
  statusTime_2_c_i = false;

  statusTime_3_a_i = false;
  statusTime_3_b_i = false;
  statusTime_3_c_i = false;

  customPickerOptions: any;




  constructor(private router: Router, private alertCtrl: AlertController) {

    // this.customPickerOptions = {
    //   buttons: [{
    //     text: 'Save',
    //     handler: () => {console.log('Clicked Save!'); return true;}
    //   }, {
    //     text: 'Log',
    //     handler: () => {
    //       console.log('Clicked Log. Do not Dismiss.');
    //       return false;
    //     }
    //   }]
    // }
    // }

  }

  ngOnInit() {
    const nowX = moment();
    const minDay1Complete = nowX.clone().add(2, 'days');
    const maxDay1Complete = minDay1Complete.clone().add(30, 'days');

    this.minDay1 = minDay1Complete.format('YYYY-MM-DD');
    this.maxDay1 = maxDay1Complete.format('YYYY-MM-DD');
    // this.maxDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
    console.log('this.minDay1 ', this.minDay1);
    console.log('this.maxDay1 ', this.maxDay1);

  }

  GuardarCita() {
    console.log('dia1', this.day_1);
    console.log('hora1', this.time_1_a_i);
    this.verificarFechas();
  } // fin GuardarCita

  verificarFechas() {
    console.log(this.time_1_a_i);
    const nowX = moment();
    console.log(nowX.format());
    const tomorrowX = nowX.clone().add(1, 'days');
    this.minDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
    this.minDay2 = nowX.clone().add(2, 'days').format('YYYY-MM-DD');
    this.minDay3 = nowX.clone().add(3, 'days').format('YYYY-MM-DD');

    // comprobando el primer dia
    if (tomorrowX.isBefore(moment(this.day_1))) {
      console.log('El primer dia VALIDO es luego de lo permitido');

      // comprobando el 2do dia
      if (moment(this.day_1).add(1, 'days').isBefore(moment(this.day_2))) {
        console.log('El Segundo dia VALIDO es luego de lo permitido');

        // comprobando el 3er dia
        if (moment(this.day_2).add(1, 'days').isBefore(moment(this.day_3))) {
          console.log('El Tercer dia VALIDO es luego de lo permitido');
          console.log('Todo Cumple');

        } else {
          console.log('El Tercer dia es Antes de lo permitido');
        }
      } else {
        console.log('El Segundo dia es Antes de lo permitido');
      }
    } else {
      console.log('El primer dia es Antes de lo permitido');
    }




  } // fin verificarFechas

  verificarHorasDia1() {
    console.log(this.time_1_a_i);
    // const nowX = moment();
    // console.log(nowX.format());
    // const tomorrowX = nowX.clone().add(1, 'days');
    // this.minDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
    // this.minDay2 = nowX.clone().add(2, 'days').format('YYYY-MM-DD');
    // this.minDay3 = nowX.clone().add(3, 'days').format('YYYY-MM-DD');

    // // comprobando la 2da Hora
    // if (moment(this.day_1).isBefore(moment(this.day_2))) {
    //   console.log('El Segundo dia VALIDO es luego de lo permitido');

    //   // comprobando el 3er dia
    //   if (moment(this.day_2).isBefore(moment(this.day_3))) {
    //     console.log('El Tercer dia VALIDO es luego de lo permitido');
    //     console.log('Todo Cumple');

    //   } else {
    //     console.log('El Tercer dia es Antes de lo permitido');
    //   }
    // } else {
    //   console.log('El Segundo dia es Antes de lo permitido');
    // }


    if (moment(this.time_1_a_i).isBefore(moment(this.time_1_b_i))) {
      if (moment(this.time_1_b_i).isBefore(moment(this.time_1_c_i))) {

      }
    } else {
      console.log('Error, la hora A es menor que la B');
    }


  } // fin verificarFechas



  // para evitar error al deshabilitar componentes
  cambioDay1() {
    this.cambioDey1();
    setTimeout(() => { this.cambioDey1(); }, 250);
  }

  cambioDay2() {
    // this.cambioDey2();
    // setTimeout(() => { this.cambioDey2(); }, 250);
    setTimeout(() => { this.cambioDey2(); }, 500);
  }

  cambioDay3() {
    this.cambioDey3();
    setTimeout(() => { this.cambioDey3(); }, 250);
    setTimeout(() => { this.cambioDey3(); }, 500);
  }

  //  funciones principales
  cambioDey1() {
    // this.day_1 = '';
    this.time_1_a_i = '';
    this.time_1_b_i = '';
    this.time_1_c_i = '';
    this.day_2 = '';
    this.time_2_a_i = '';
    this.time_2_b_i = '';
    this.time_2_c_i = '';
    this.day_3 = '';
    this.time_3_a_i = '';
    this.time_3_b_i = '';
    this.time_3_c_i = '';

    this.statusDay1 = true;
    this.statusDay2 = false;
    this.statusDay3 = false;

    this.statusTime_1_a_i = true;
    this.statusTime_1_b_i = false;
    this.statusTime_1_c_i = false;
    this.statusTime_2_a_i = false;
    this.statusTime_2_b_i = false;
    this.statusTime_2_c_i = false;
    this.statusTime_3_a_i = false;
    this.statusTime_3_b_i = false;
    this.statusTime_3_c_i = false;
    console.log('Cambio el Dia1');
  }

  cambioDey2() {
    // this.day_1 = '';
    // this.time_1_a_i = '';
    // this.time_1_b_i = '';
    // this.time_1_c_i = '';
    // this.day_2 = '';
    this.time_2_a_i = '';
    this.time_2_b_i = '';
    this.time_2_c_i = '';
    this.day_3 = '';
    this.time_3_a_i = '';
    this.time_3_b_i = '';
    this.time_3_c_i = '';

    this.statusDay1 = true;
    this.statusDay2 = true;
    this.statusDay3 = false;

    this.statusTime_1_a_i = true;
    this.statusTime_1_b_i = true;
    this.statusTime_1_c_i = true;

    this.statusTime_2_a_i = true;
    this.statusTime_2_b_i = false;
    this.statusTime_2_c_i = false;

    this.statusTime_3_a_i = false;
    this.statusTime_3_b_i = false;
    this.statusTime_3_c_i = false;

    console.log('Cambio el Dia2');
  }

  cambioDey3() {
    // this.day_1 = '';
    // this.time_1_a_i = '';
    // this.time_1_b_i = '';
    // this.time_1_c_i = '';
    // this.day_2 = '';
    // this.time_2_a_i = '';
    // this.time_2_b_i = '';
    // this.time_2_c_i = '';
    // this.day_3 = '';
    this.time_3_a_i = '';
    this.time_3_b_i = '';
    this.time_3_c_i = '';

    this.statusDay1 = true;
    this.statusDay2 = true;
    this.statusDay3 = true;
    this.statusTime_1_a_i = true;
    this.statusTime_1_b_i = true;
    this.statusTime_1_c_i = true;
    this.statusTime_2_a_i = true;
    this.statusTime_2_b_i = true;
    this.statusTime_2_c_i = true;

    this.statusTime_3_a_i = true;
    this.statusTime_3_b_i = false;
    this.statusTime_3_c_i = false;
    console.log('Cambio el Dia3');
  }


  cambioT1a() { this.statusTime_1_b_i = true; }
  cambioT1b() { this.statusTime_1_c_i = true; }
  cambioT1c() { this.statusDay2 = true; }

  cambioT2a() { this.statusTime_2_b_i = true; }
  cambioT2b() { this.statusTime_2_c_i = true; }
  cambioT2c() { this.statusDay3 = true; }

  cambioT3a() { this.statusTime_3_b_i = true; }
  cambioT3b() { this.statusTime_3_c_i = true; }
  cambioT3c() { console.log('exitooooo Terminó todo'); }



  cancelando() {
    console.log('cancelandoooooooooooooo');
  }




}
