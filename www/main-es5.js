(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/configuration/configuration.module": [
		"./src/app/pages/configuration/configuration.module.ts",
		"default~pages-configuration-configuration-module~pages-create-visit-create-visit-module",
		"pages-configuration-configuration-module"
	],
	"./pages/create-visit/create-visit.module": [
		"./src/app/pages/create-visit/create-visit.module.ts",
		"default~pages-configuration-configuration-module~pages-create-visit-create-visit-module",
		"pages-create-visit-create-visit-module"
	],
	"./pages/favorites/favorites.module": [
		"./src/app/pages/favorites/favorites.module.ts",
		"pages-favorites-favorites-module"
	],
	"./pages/house-detail/house-detail.module": [
		"./src/app/pages/house-detail/house-detail.module.ts",
		"pages-house-detail-house-detail-module"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"pages-login-login-module"
	],
	"./pages/new-house-upload/new-house-upload.module": [
		"./src/app/pages/new-house-upload/new-house-upload.module.ts",
		"pages-new-house-upload-new-house-upload-module"
	],
	"./pages/new-house/new-house.module": [
		"./src/app/pages/new-house/new-house.module.ts",
		"pages-new-house-new-house-module"
	],
	"./pages/register/register.module": [
		"./src/app/pages/register/register.module.ts",
		"pages-register-register-module"
	],
	"./pages/search/search.module": [
		"./src/app/pages/search/search.module.ts",
		"pages-search-search-module"
	],
	"./pages/view-close-detail/view-close-detail.module": [
		"./src/app/pages/view-close-detail/view-close-detail.module.ts",
		"pages-view-close-detail-view-close-detail-module"
	],
	"./pages/view-close/view-close.module": [
		"./src/app/pages/view-close/view-close.module.ts",
		"pages-view-close-view-close-module"
	],
	"./pages/view-open-detail/view-open-detail.module": [
		"./src/app/pages/view-open-detail/view-open-detail.module.ts",
		"pages-view-open-detail-view-open-detail-module"
	],
	"./pages/view-open/view-open.module": [
		"./src/app/pages/view-open/view-open.module.ts",
		"pages-view-open-view-open-module"
	],
	"./pages/view-wait-detail/view-wait-detail.module": [
		"./src/app/pages/view-wait-detail/view-wait-detail.module.ts",
		"pages-view-wait-detail-view-wait-detail-module"
	],
	"./pages/view-wait/view-wait.module": [
		"./src/app/pages/view-wait/view-wait.module.ts",
		"pages-view-wait-view-wait-module"
	],
	"./pages/view360/view360.module": [
		"./src/app/pages/view360/view360.module.ts",
		"pages-view360-view360-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm-es5 lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm-es5 lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-controller_8.entry.js",
		"common",
		3
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-ios.entry.js",
		"common",
		4
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-md.entry.js",
		"common",
		5
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-alert-ios.entry.js",
		"common",
		6
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-alert-md.entry.js",
		"common",
		7
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-app_8-ios.entry.js",
		0,
		"common",
		8
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-app_8-md.entry.js",
		0,
		"common",
		9
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-avatar_3-ios.entry.js",
		"common",
		10
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-avatar_3-md.entry.js",
		"common",
		11
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-back-button-ios.entry.js",
		"common",
		12
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-back-button-md.entry.js",
		"common",
		13
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-backdrop-ios.entry.js",
		1,
		"common",
		14
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-backdrop-md.entry.js",
		1,
		"common",
		15
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-button_2-ios.entry.js",
		"common",
		16
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-button_2-md.entry.js",
		"common",
		17
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-card_5-ios.entry.js",
		"common",
		18
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-card_5-md.entry.js",
		"common",
		19
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-checkbox-ios.entry.js",
		"common",
		20
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-checkbox-md.entry.js",
		"common",
		21
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-chip-ios.entry.js",
		"common",
		22
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-chip-md.entry.js",
		"common",
		23
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-col_3.entry.js",
		24
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-datetime_3-ios.entry.js",
		"common",
		25
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-datetime_3-md.entry.js",
		"common",
		26
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-fab_3-ios.entry.js",
		"common",
		27
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-fab_3-md.entry.js",
		"common",
		28
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-img.entry.js",
		29
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-infinite-scroll_2-ios.entry.js",
		"common",
		30
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-infinite-scroll_2-md.entry.js",
		"common",
		31
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-input-ios.entry.js",
		"common",
		32
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-input-md.entry.js",
		"common",
		33
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item-option_3-ios.entry.js",
		"common",
		34
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item-option_3-md.entry.js",
		"common",
		35
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item_8-ios.entry.js",
		"common",
		36
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item_8-md.entry.js",
		"common",
		37
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-loading-ios.entry.js",
		"common",
		38
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-loading-md.entry.js",
		"common",
		39
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-menu_4-ios.entry.js",
		1,
		"common",
		40
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-menu_4-md.entry.js",
		1,
		"common",
		41
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-modal-ios.entry.js",
		0,
		"common",
		42
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-modal-md.entry.js",
		0,
		"common",
		43
	],
	"./ion-nav_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-nav_4.entry.js",
		0,
		"common",
		44
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-popover-ios.entry.js",
		0,
		"common",
		45
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-popover-md.entry.js",
		0,
		"common",
		46
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-progress-bar-ios.entry.js",
		"common",
		47
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-progress-bar-md.entry.js",
		"common",
		48
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-radio_2-ios.entry.js",
		"common",
		49
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-radio_2-md.entry.js",
		"common",
		50
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-range-ios.entry.js",
		"common",
		51
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-range-md.entry.js",
		"common",
		52
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-refresher_2-ios.entry.js",
		"common",
		53
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-refresher_2-md.entry.js",
		"common",
		54
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-reorder_2-ios.entry.js",
		"common",
		55
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-reorder_2-md.entry.js",
		"common",
		56
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-ripple-effect.entry.js",
		57
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-route_4.entry.js",
		"common",
		58
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-searchbar-ios.entry.js",
		"common",
		59
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-searchbar-md.entry.js",
		"common",
		60
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-segment_2-ios.entry.js",
		"common",
		61
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-segment_2-md.entry.js",
		"common",
		62
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-select_3-ios.entry.js",
		"common",
		63
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-select_3-md.entry.js",
		"common",
		64
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-slide_2-ios.entry.js",
		"common",
		65
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-slide_2-md.entry.js",
		"common",
		66
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-spinner.entry.js",
		"common",
		67
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-split-pane-ios.entry.js",
		68
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-split-pane-md.entry.js",
		69
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab-bar_2-ios.entry.js",
		"common",
		70
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab-bar_2-md.entry.js",
		"common",
		71
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab_2.entry.js",
		2
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-text.entry.js",
		"common",
		72
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-textarea-ios.entry.js",
		"common",
		73
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-textarea-md.entry.js",
		"common",
		74
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toast-ios.entry.js",
		"common",
		75
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toast-md.entry.js",
		"common",
		76
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toggle-ios.entry.js",
		"common",
		77
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toggle-md.entry.js",
		"common",
		78
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-virtual-scroll.entry.js",
		79
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm-es5 lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\n  <ion-split-pane>\n    <ion-menu type=\"overlay\" menuId='mymenu' >\n      <!-- <ion-header> -->\n        <!-- <ion-grid> -->\n          <!-- <ion-row justify-content-center align-items-center> -->\n            <!-- <ion-title>TiendaInmueble</ion-title> -->\n            <!-- <ion-label padding>TiendaInmueble</ion-label> -->\n          <!-- </ion-row> -->\n          <!-- <ion-row>\n            <ion-toolbar>\n            <ion-img src=\"../assets/myimages/logo.jpg\"></ion-img>\n            </ion-toolbar>\n          </ion-row> -->\n          <!-- <ion-row justify-content-center align-items-center> -->\n            <!-- <ion-toolbar> -->\n            <!-- <ion-chip>\n              <ion-avatar>\n                <img src=\"../assets/myimages/user02.png\" />\n              </ion-avatar>\n              <ion-label>{{ email }}</ion-label>\n            </ion-chip> -->\n            <!-- </ion-toolbar> -->\n          <!-- </ion-row> -->\n        <!-- </ion-grid> -->\n      <!-- </ion-header> -->\n      <ion-content style=\"--background:var(--ion-color-primary)\">\n        <ion-list  style=\"--background:var(--ion-color-primary)\" class=\"ion-no-padding\">\n          <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of appPages\"  style=\"--background:var(--ion-color-primary)\">\n            <ion-item [routerDirection]=\"'root'\" [routerLink]=\"[p.url]\" style=\"--background:var(--ion-color-primary)\">\n              <ion-icon slot=\"start\" [name]=\"p.icon\"  style=\"color: white;\"></ion-icon>\n              <ion-label style=\"color: white;\">\n                {{ p.title }}\n              </ion-label> \n            </ion-item>\n          </ion-menu-toggle>\n        </ion-list>\n        <!-- <ion-list routerLink=\"/login\" *ngIf=\"btniniciar\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-item>\n              <ion-icon slot=\"start\"></ion-icon>\n              <ion-label expand=\"full\">\n                INICIA SESIÓN\n              </ion-label>\n            </ion-item>\n          </ion-menu-toggle>\n        </ion-list> -->\n      </ion-content>\n    </ion-menu>\n    <ion-router-outlet main></ion-router-outlet>\n  </ion-split-pane>\n</ion-app>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
    },
    {
        path: "home",
        loadChildren: function () { return __webpack_require__.e(/*! import() | home-home-module */ "home-home-module").then(__webpack_require__.bind(null, /*! ./home/home.module */ "./src/app/home/home.module.ts")).then(function (m) { return m.HomePageModule; }); }
    },
    {
        path: "list",
        loadChildren: function () { return __webpack_require__.e(/*! import() | list-list-module */ "list-list-module").then(__webpack_require__.bind(null, /*! ./list/list.module */ "./src/app/list/list.module.ts")).then(function (m) { return m.ListPageModule; }); }
    },
    {
        path: "house-detail",
        loadChildren: "./pages/house-detail/house-detail.module#HouseDetailPageModule"
    },
    { path: "login", loadChildren: "./pages/login/login.module#LoginPageModule" },
    {
        path: "register",
        loadChildren: "./pages/register/register.module#RegisterPageModule"
    },
    {
        path: "view-open",
        loadChildren: "./pages/view-open/view-open.module#ViewOpenPageModule"
    },
    {
        path: "view-open-detail",
        loadChildren: "./pages/view-open-detail/view-open-detail.module#ViewOpenDetailPageModule"
    },
    {
        path: "view-wait",
        loadChildren: "./pages/view-wait/view-wait.module#ViewWaitPageModule"
    },
    {
        path: "view-wait-detail",
        loadChildren: "./pages/view-wait-detail/view-wait-detail.module#ViewWaitDetailPageModule"
    },
    {
        path: "view-close",
        loadChildren: "./pages/view-close/view-close.module#ViewClosePageModule"
    },
    {
        path: "view-close-detail",
        loadChildren: "./pages/view-close-detail/view-close-detail.module#ViewCloseDetailPageModule"
    },
    {
        path: "create-visit",
        loadChildren: "./pages/create-visit/create-visit.module#CreateVisitPageModule"
    },
    {
        path: "new-house",
        loadChildren: "./pages/new-house/new-house.module#NewHousePageModule"
    },
    {
        path: "new-house-upload",
        loadChildren: "./pages/new-house-upload/new-house-upload.module#NewHouseUploadPageModule"
    },
    { path: 'configuration', loadChildren: './pages/configuration/configuration.module#ConfigurationPageModule' },
    { path: 'favorites', loadChildren: './pages/favorites/favorites.module#FavoritesPageModule' },
    { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' },
    { path: 'view360', loadChildren: './pages/view360/view360.module#View360PageModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/mysevice.service */ "./src/app/services/mysevice.service.ts");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/variables.service */ "./src/app/services/variables.service.ts");









var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, http, st, mys, // private http: HttpClient
    myv, // private http: HttpClient
    menuController) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.http = http;
        this.st = st;
        this.mys = mys;
        this.myv = myv;
        this.menuController = menuController;
        // user: any;
        // user = window['user'];
        this.btniniciar = true;
        this.email = 'Usuario';
        // tslint:disable-next-line: member-ordering
        this.appPages = [
            {
                title: 'HOME',
                url: '/home',
                icon: 'images'
            },
            {
                title: 'Favoritos',
                url: '/favorites',
                icon: 'heart'
            },
            {
                title: 'Visitas',
                url: '/view-open',
                icon: 'copy'
            }
        ];
        this.initializeApp();
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log('ngOnInit desde app.components solo la primera vez...');
        // this.mys.loadingPresent();
        console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
        var ururur = this.myv.getGlobal();
        // console.log(ururur);
        this.mys.getAndSetInmuebles();
        // let hfhfhfh = this.myv.getGlobal()
        // console.log(hfhfhfh);
        // console.log(hfhfhfh.lenght);
        // this.myv.getAndSetInmuebles()
        // if (this.mys.isSetInmuebles()) {
        //   console.log('sssssssssssi hay inmuebles');
        // } else {
        //   console.log('nnnnnnnnnnnnnno hay inmuebles');
        // }
        console.log('cargando inmuebeles en Global');
    };
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        // this.mys.loadingPresent();
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_4__["SplashScreen"] },
        { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__["StatusBar"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__["MyseviceService"] },
        { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_8__["VariablesService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_4__["SplashScreen"],
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__["StatusBar"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _services_mysevice_service__WEBPACK_IMPORTED_MODULE_7__["MyseviceService"],
            _services_variables_service__WEBPACK_IMPORTED_MODULE_8__["VariablesService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");













// import { LoadingController } from '@ionic/angular';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
            entryComponents: [],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_12__["PipesModule"],
                _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["IonicStorageModule"].forRoot({
                    name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                }),
            ],
            providers: [
                _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
                _services_variables_service__WEBPACK_IMPORTED_MODULE_11__["VariablesService"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
                // LoadingController,
                { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/pipes/filtro.pipe.ts":
/*!**************************************!*\
  !*** ./src/app/pipes/filtro.pipe.ts ***!
  \**************************************/
/*! exports provided: FiltroPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroPipe", function() { return FiltroPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


// import { Title } from '@angular/platform-browser';
var FiltroPipe = /** @class */ (function () {
    function FiltroPipe() {
    }
    FiltroPipe.prototype.transform = function (arreglo, texto, columna) {
        if (texto === '') {
            return arreglo;
        } /// para la lista completa mientras no haya busqueda
        // if (texto === '') { return []; }              /// para la lista vacía mientras no haya busqueda
        texto = texto.toLowerCase();
        return arreglo.filter(function (item) {
            return item[columna].toLowerCase().includes(texto);
        });
    };
    FiltroPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filtro'
        })
    ], FiltroPipe);
    return FiltroPipe;
}());



/***/ }),

/***/ "./src/app/pipes/pipes.module.ts":
/*!***************************************!*\
  !*** ./src/app/pipes/pipes.module.ts ***!
  \***************************************/
/*! exports provided: PipesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PipesModule", function() { return PipesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filtro_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filtro.pipe */ "./src/app/pipes/filtro.pipe.ts");



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_filtro_pipe__WEBPACK_IMPORTED_MODULE_2__["FiltroPipe"]],
            exports: [_filtro_pipe__WEBPACK_IMPORTED_MODULE_2__["FiltroPipe"]]
        })
    ], PipesModule);
    return PipesModule;
}());



/***/ }),

/***/ "./src/app/services/mysevice.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/mysevice.service.ts ***!
  \**********************************************/
/*! exports provided: MyseviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyseviceService", function() { return MyseviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");







var MyseviceService = /** @class */ (function () {
    function MyseviceService(httpClient, st, myv, loadingCtrl, toastcontroller) {
        this.httpClient = httpClient;
        this.st = st;
        this.myv = myv;
        this.loadingCtrl = loadingCtrl;
        this.toastcontroller = toastcontroller;
        this.view360url = '';
        this.isLoading = false;
        // variables ///////////////////
        // UrlBase = 'http://192.168.16.106:8081';
        this.UrlBase = 'http://192.168.16.113:8081';
        this.VarGlobal = 'mydata'; // en  el index
        this.usuario = { email: '', password: '' };
        this.urlConfirm = '/confirm?user=';
        this.urlPass = '&pass=';
    }
    MyseviceService.prototype.loadingPresent = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Cargando ...',
                                spinner: 'circles'
                            }).then(function (a) {
                                a.present().then(function () {
                                    console.log('loading presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort laoding'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    MyseviceService.prototype.loadingDismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingCtrl.dismiss().then(function () { return console.log('loading dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ////////////////////////////////////////////////////////////////////
    //////////////////////////// API ///////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    MyseviceService.prototype.getinmuebles = function () {
        var _this = this;
        // guardo los inmuebles en storage
        this.httpClient
            .get(this.UrlBase + '/inmuebles')
            .subscribe(function (data) {
            _this.st.set('inmuebles', data);
            console.log('Guardado inmuebles desde Services ' + data);
        });
    };
    ////////////////////////////////////////////////////////////////////
    MyseviceService.prototype.usersigned = function () {
        this.st.get('user').then(function (valor) {
            if (valor) {
                console.log('Usuario registrado - desde service');
                return true;
            }
            else {
                console.log('Usuario NO registrado - desde service');
                return false;
            }
        });
    };
    MyseviceService.prototype.getemail = function () {
        this.st.get('user').then(function (valor) {
            if (valor) {
                return valor[0].user;
            }
            else {
                console.log(valor);
                return 'no hay acceso';
            }
        });
    };
    MyseviceService.prototype.getrol = function () {
        this.st.get('user').then(function (valor) {
            if (valor) {
                return valor[0].rol;
            }
            else {
                // console.log(valor);
                return 'no hay acceso';
            }
        });
    };
    ////////////////////////////////////////////////////////////////////
    //////////////////////////// GENERAL ///////////////////////////////
    MyseviceService.prototype.getAndSetInmuebles = function () {
        var _this = this;
        var urlFinal = this.UrlBase + '/inmuebles';
        console.log('Getting la lista de inmuebles desde: ' + urlFinal);
        this.httpClient.get(urlFinal).subscribe(function (data) {
            // console.log(data);
            _this.myv.setInmuebles(data);
            // this.myv.setInmuebles(data)
        });
    };
    MyseviceService.prototype.confirmUser = function (user) {
        var _this = this;
        var urlFinal = this.UrlBase +
            this.urlConfirm +
            user.email +
            this.urlPass +
            user.password;
        console.log('Confirmando usuario con : ' + urlFinal);
        this.httpClient.get(urlFinal).subscribe(function (data) {
            try {
                // caso si el usuario SI se Logeo
                if (data[0].phone) {
                    _this.myv.setUserSigned(data[0]);
                    console.log('El usuario se logeo satisfactoriamente desde myService.');
                }
            }
            catch (error) {
                // caso si el usuario NO se Logeo
                console.log('El usuario NO se logeo desde myService.');
            }
        });
    };
    ////////////////////////////////////////////////////////////////////////
    MyseviceService.prototype.toast_mostrar = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastcontroller.create({
                            message: message,
                            animated: true,
                            color: 'primary',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyseviceService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
        { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_4__["VariablesService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] }
    ]; };
    MyseviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
            _services_variables_service__WEBPACK_IMPORTED_MODULE_4__["VariablesService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], MyseviceService);
    return MyseviceService;
}()); /////////// fin my service



/***/ }),

/***/ "./src/app/services/variables.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/variables.service.ts ***!
  \***********************************************/
/*! exports provided: VariablesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariablesService", function() { return VariablesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var VariablesService = /** @class */ (function () {
    function VariablesService(httpClient) {
        this.httpClient = httpClient;
        // variables estaticas
        this.UrlBaseGeneral = 'http://192.168.16.106:8081/';
        this.VarGlobal = 'mydata'; // en  el index.html
        // variables ///////////////////
        this.urlinmuebles = 'http://localhost:8081/user?id=3';
        this.usuario = { email: '', password: '' };
        // url='http://localhost:8081/confirm?user=user03&pass=pass03';
        this.urlConfirm = 'confirm?user=';
        this.urlPass = '&pass=';
    }
    ////////////////////////////////////////////////////////////////////////////////
    ///////////////////////// Variables Globales ///////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // public inmuebles = [];
    // public inmueble = {};
    // public user = {};
    // public citas = [];
    // public cita = {};
    // public inmuebles: Array<Inmueble>;
    // public inmueble: Inmueble;
    // public user: User;
    // public citas: Array<Cita>;
    // public cita: Cita;
    //  inmuebles: any;
    // public inmueble: any;
    // public user: any;
    // public citas: Array<any>;
    // public cita: any;
    ////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Funciones ////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // public getInmuebles(): Array<Inmueble> { return this.inmuebles; }
    // public setInmuebles(inmueblesX: Array<Inmueble>): void { this.inmuebles = inmueblesX; }
    // public isSetInmuebles(): boolean {
    //   console.log(this.inmuebles);
    //   console.log(this.inmuebles);
    //   if (this.inmuebles.length > 0) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    // public getInmueble(): Inmueble { return this.inmueble; }
    // public setInmueble(inmuebleX: Inmueble): void { this.inmueble = inmuebleX; }
    // public isSetInmueble(): boolean {
    //   if (this.inmueble.hasOwnProperty('id')) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    // public getUserSigned(): User { return this.user; }
    // public setUserSigned(userX: User): void { this.user = userX; }
    // public isSetUserSigned(): boolean {
    //   if (this.inmueble.hasOwnProperty('id')) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    // public getCitas(): Array<Cita> { return this.citas; }
    // public setCitas(citasX: Array<Cita>): void { this.citas = citasX; }
    // public isSetCitas(): boolean {
    //   if (this.citas.length > 0) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    // public getCita(): Cita { return this.cita; }
    // public setCita(citaX: Cita): void { this.cita = citaX; }
    // public isSetCita(): boolean {
    //   if (this.inmueble.hasOwnProperty('id')) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
    // public getAndSetInmuebles(): void {
    //   const urlFinal = this.UrlBaseGeneral + 'inmuebles';
    //   console.log('Getting la lista de inmuebles desde: ' + urlFinal);
    //   this.httpClient.get(urlFinal).subscribe(data => {
    //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy1');
    //     console.log(data);
    //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy2');
    //     // console.log(data);
    //     // console.log(data);
    //     // this.myv.setInmuebles(data);
    //     this.inmueble = data
    //     console.log(this.inmueble);
    //     console.log('yyyyyyyyyyyyyyyyyyyyyyyyyy3');
    //     console.log(this.inmueble);
    //     console.log(this.inmueble);
    //   });
    // }
    ////////////////////////////////////////////////////////////////////////////////
    VariablesService.prototype.getGlobal = function () {
        return window[this.VarGlobal];
    };
    VariablesService.prototype.isUserSigned = function () {
        var mydata = window[this.VarGlobal];
        console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhh1');
        console.log(mydata);
        console.log(mydata.inmuebles);
        console.log(mydata.user);
        console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhh2');
        if (mydata.user) {
            return true;
        }
        else {
            return false;
        }
    };
    VariablesService.prototype.removeUserSigned = function () {
        var mydata = window[this.VarGlobal];
        delete mydata.user;
        window[this.VarGlobal] = mydata;
    };
    VariablesService.prototype.setUserSigned = function (NewUser) {
        var mydata = window[this.VarGlobal];
        mydata.user = NewUser;
        window[this.VarGlobal] = mydata;
    };
    VariablesService.prototype.getUserSigned = function () {
        var mydata = window[this.VarGlobal];
        return mydata.user;
    };
    VariablesService.prototype.isSetUserSigned = function () {
        var mydata = window[this.VarGlobal];
        if (mydata.user) {
            return true;
        }
        else {
            return false;
        }
    };
    VariablesService.prototype.getInmuebleDetailId = function (ninmueble) {
        var mydata = window[this.VarGlobal];
        var misInmuebles = mydata.inmuebles;
        var result = misInmuebles.filter(function (cinmueble) { return cinmueble.id === ninmueble; });
        return result[0];
    };
    VariablesService.prototype.setInmuebles = function (inmueblesNew) {
        var mydata = window[this.VarGlobal];
        mydata.inmuebles = inmueblesNew;
        window[this.VarGlobal] = mydata;
    };
    VariablesService.prototype.setInmueble = function (inmuebleNew) {
        var mydata = window[this.VarGlobal];
        mydata.inmueble = inmuebleNew;
        window[this.VarGlobal] = mydata;
    };
    VariablesService.prototype.getInmuebles = function () {
        var mydata = window[this.VarGlobal];
        return mydata.inmuebles;
    };
    VariablesService.prototype.getInmueble = function () {
        var mydata = window[this.VarGlobal];
        var mydata2 = mydata.inmueble;
        return mydata2;
    };
    VariablesService.prototype.isSetInmueble = function () {
        var mydata = window[this.VarGlobal];
        if (mydata.inmueble) {
            return true;
        }
        else {
            return false;
        }
    };
    VariablesService.prototype.setRemoveInmueble = function () {
        var mydata = window[this.VarGlobal];
        delete mydata.inmueble;
    };
    VariablesService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    VariablesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], VariablesService);
    return VariablesService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    url_base: 'http://localhost:3000'
    //////////////  para llamar
    //  import { environment } from '../../environments/environment';
    //  no es necesario contructor
    //  environment.url_base
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\app\Documents\--misProyectos\tienda-inmueble-02-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map