(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-house-detail-house-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/house-detail/house-detail.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/house-detail/house-detail.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\"> \n    <ion-buttons > \n      <ion-back-button style=\"color: white;\" defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\"> Detalles </ion-title>\n    <ion-buttons slot=\"end\" style=\"color: white;\">\n      <ion-menu-button> </ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content justify-content-center align-items-center style=\"--background:var(--ion-color-light)\">\n\n  <!-- <ion-card>\n    <ion-img src=\"{{ this.urlimage + this.inmueble.pics[0] }}\" class=\"complemento-imagen\"></ion-img>\n  </ion-card> -->\n\n  <ion-card > \n    <ion-slides pager=\"ios\" [options]=\"options2d\" class=\"complemento-imagen\">\n      <ion-slide *ngFor=\"let item of this.inmueble.pics\" style=\"width: 100%; height: 100%;\">\n        <img src=\"{{ this.urlimage + item }}\"> \n      </ion-slide>\n    </ion-slides>\n  </ion-card>\n\n  <ion-grid fixed style=\"margin-top: -15px\">\n    <ion-row justify-content-center align-items-center>\n      <ion-text color=\"secondary\">\n        <h1><strong>{{ inmueble.name }}</strong></h1>\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <div\n    style=\"height: 50px;background-color: rgb(238, 238, 238);border-radius: 20px; margin-left: 10%; margin-right: 10% ;margin-top: 5px \">\n    <div style=\"width: 50%; height: 100%; text-align: center;float:left;\">\n      <ion-text color=\"secondary\">\n        <h4 style=\"line-height: 15px\">\n          <strong>Precio</strong>\n        </h4>\n      </ion-text>\n    </div>\n    <div\n      style=\"width: 50%; height: 100%; background-color: rgb(191,147,84) ; float:right; border-radius: 20px;; text-align: center; margin-top: 0px\">\n      <ion-text color=\"light\">\n        <h2 style=\"line-height: 8px\">\n          <strong>{{ this.inmueble.price }} $</strong>\n        </h2>\n      </ion-text>\n    </div> \n  </div>\n\n  \n<!-- Ubicacion -->\n  <ion-grid fixed style=\"margin-top: 20px;\">\n    <ion-row>\n      <ion-text color=\"secondary\">\n        {{ inmueble.place }}\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <!-- Descripcion -->\n  <ion-grid fixed style=\"margin-top: 10px ; margin-bottom: 10px\">\n    <ion-row>\n      <ion-text color=\"secondary\">\n        {{ inmueble.description }}\n      </ion-text>\n    </ion-row>\n  </ion-grid>\n\n  <!-- Slider 360 -->\n  <div style=\"padding-top: 15px; padding-bottom: 15px; position: relative;\" *ngIf=\"array360url.length >0\">\n\n      <div style=\"position: absolute;z-index: 2; left: 50%; margin-left: -50px; bottom: 86px; \"> \n        <img src='../../../assets/myimages/icon360.jpg' style=\"height: 50px;text-align: center; \">\n      </div>\n  \n      <ion-card style=\"background-color:  rgb(238, 238, 238);text-align: center;\"> \n        <ion-slides [options]=\"options360\" class=\"complemento-imagen-360\">\n          <ion-slide *ngFor=\"let item of array360url; let i = index\" \n            class=\"slide360\" (click)=\"view360url(item)\">\n            <div class=\"divBtn\">\n              {{i+1}}\n            </div>\n          </ion-slide>\n        </ion-slides>\n      </ion-card>\n  \n    </div>\n\n\n  <!-- Features and Amenities -->\n  <ion-item-group>\n    <!-- Features -->\n    <div style=\"height: fit-content;  background-color: rgb(238, 238, 238);padding-bottom: 30px;\">\n\n      <ion-grid fixed>\n        <ion-row justify-content-center align-items-center>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Área construida</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{this.inmueble.areac}}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item class=\"ion-padding-\">\n              <ion-label style=\"color: #72726e\">Área terreno</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.areat\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Parking</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.parking\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Baños</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.bathroom\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Antiguedad</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.antiquity\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n          <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n            <ion-item>\n              <ion-label style=\"color: #72726e\">Pisos</ion-label>\n              <ion-note slot=\"end\" color=\"secondary\">{{\n                this.inmueble.floors\n              }}</ion-note>\n            </ion-item>\n          </ion-col>\n          <!-- ///////////////////////////////////////////////////////////////// -->\n        </ion-row>\n      </ion-grid>\n\n    </div>\n\n    <!-- Amenities -->\n    <ion-grid fixed>\n      <ion-row justify-content-center align-items-center>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Amoblada</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Wifi</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Jardin</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Piscina</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Salón de juegos</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Vigilancia privada</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n        <!-- ///////////////////////////////////////////////////////////////// -->\n        <ion-col size=\"12\" size-lg=\"4\" size-md=\"6\" size-sm=\"6\" size-xs=\"12\" class=\"ion-padding-start\">\n          <ion-item>\n            <ion-label style=\"color: #72726e\">Circuito cerrado</ion-label>\n            <ion-icon slot=\"end\" name=\"checkmark-circle-outline\" color=\"tertiary\"></ion-icon>\n            <!-- <ion-note slot=\"end\" color=\"primary\">{{ this.inmueble.areac }}</ion-note> -->\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item-group>\n</ion-content>\n\n<ion-footer style=\"text-align: center;\">\n  <ion-toolbar color=\"primary\">\n\n    <!-- <a href=\"whatsapp://send?phone=584242332373?text=Me%20gustaría%20saber%20el%20precio%20del%20coche\">Link WhatsApp</a> -->\n    <ion-button style=\"text-align: center; margin-left: 5%; \" slot=\"start\" color=\"secondary\" (click)=\"whatsapp()\">\n      <ion-icon slot=\"icon-only\" slot=\"start\" name=\"logo-whatsapp\"\n        style=\"color: burlywood ;padding-left: 0px; padding-right: 0px; margin-left: 0px; margin-right: 0px; \">\n      </ion-icon>\n    </ion-button>\n\n    <ion-button style=\"text-align: center;     width: -webkit-fill-available; \" color=\"secondary\" routerLink=\"/create-visit\">\n      <ion-title\n        style=\"color: white;padding-left: 0px; padding-right: 0px; margin-left: 0px; margin-right: 0px; text-transform: none; font-size: 10;\">\n        Agendar Visita</ion-title>\n    </ion-button>\n\n    <ion-button style=\"text-align: center; margin-right: 5%; \" slot=\"end\" color=\"secondary\" (click)=\"tourVirtual()\">\n      <ion-icon slot=\"icon-only\" slot=\"end\" name=\"md-easel\"\n        style=\"color: burlywood;padding-left: 0px; padding-right: 0px; margin-left: 0px; margin-right: 0px;\"></ion-icon>\n    </ion-button>\n\n\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.module.ts ***!
  \***********************************************************/
/*! exports provided: HouseDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseDetailPageModule", function() { return HouseDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _house_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./house-detail.page */ "./src/app/pages/house-detail/house-detail.page.ts");







const routes = [
    {
        path: '',
        component: _house_detail_page__WEBPACK_IMPORTED_MODULE_6__["HouseDetailPage"]
    }
];
let HouseDetailPageModule = class HouseDetailPageModule {
};
HouseDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_house_detail_page__WEBPACK_IMPORTED_MODULE_6__["HouseDetailPage"]]
    })
], HouseDetailPageModule);



/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item {\n  --ion-background-color: transparent !important;\n}\n\n.complemento-imagen {\n  height: 50vh;\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n\n.complemento-imagen-360 {\n  height: 50px;\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n  margin-top: 49px;\n  margin-bottom: 5px;\n}\n\n.divBtn {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  color: white;\n  font-size: 30px;\n}\n\n.slide360 {\n  background-color: gray;\n  border-radius: 50px;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n\n.slide360:active {\n  opacity: 0.5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG91c2UtZGV0YWlsL0M6XFxVc2Vyc1xcYXBwXFxEb2N1bWVudHNcXC0tbWlzUHJveWVjdG9zXFx0aWVuZGEtaW5tdWVibGUtMDItYXBwL3NyY1xcYXBwXFxwYWdlc1xcaG91c2UtZGV0YWlsXFxob3VzZS1kZXRhaWwucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob3VzZS1kZXRhaWwvaG91c2UtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFXQTtFQUVFLDhDQUFBO0FDWEY7O0FEY0E7RUEyQ0UsWUFBQTtFQUNBLFdBQUE7RUFFQSx5QkFBQTtLQUFBLHNCQUFBO0FDdERGOztBRHlEQTtFQTJDRSxZQUFBO0VBQ0EsV0FBQTtFQUVBLHlCQUFBO0tBQUEsc0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDakdGOztBRG9HQTtFQUNFLHdCQUFBO1VBQUEsdUJBQUE7RUFBeUIseUJBQUE7VUFBQSxtQkFBQTtFQUFxQixZQUFBO0VBQWMsZUFBQTtBQzlGOUQ7O0FEa0dBO0VBQ0Usc0JBQUE7RUFBdUIsbUJBQUE7RUFBb0IsZUFBQTtFQUFnQixrQkFBQTtBQzVGN0Q7O0FEK0ZBO0VBRUUsWUFBQTtBQzdGRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvdXNlLWRldGFpbC9ob3VzZS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWl0ZW0ge1xyXG4vLyAtLWJvcmRlci1jb2xvcjogXCJkb3JhZG9cIjtcclxuLy8gLS1wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4vLyAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbi8vIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4vLyAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbi8vIH1cclxuXHJcbi8vIC5sYWJlbHtcclxuLy8gICBjb2xvcjogIzcyNzI2ZVwiXHJcbi8vIH1cclxuaW9uLWl0ZW0ge1xyXG4gIC8vIC0taW9uLWJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JleTtcclxuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY29tcGxlbWVudG8taW1hZ2VuIHtcclxuICAvLyAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG4gIC8vIC1tb3otYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgLy8gYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgLy8gb3BhY2l0eTogMC45NTtcclxuICAvLyAvLyBwYWRkaW5nOiAyODBweDtcclxuICAvLyBib3JkZXI6IG5vbmU7XHJcbiAgLy8gLy8gZm9udDogbm9ybWFsIDE2cHgvMSBcIlRpbWVzIE5ldyBSb21hblwiLCBUaW1lcywgc2VyaWY7XHJcbiAgLy8gY29sb3I6IGJsYWNrO1xyXG4gIC8vIC1vLXRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIC8vIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIC8vIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KFxyXG4gIC8vICAgNzkuNjgzNTMyMzE1Njg5MzVkZWcsXHJcbiAgLy8gICByZ2JhKDE1LCAyNiwgMzUsIDEpIDAsXHJcbiAgLy8gICByZ2JhKDExNSwgMTc3LCAyMzEsIDAuNSkgMjQlLFxyXG4gIC8vICAgcmdiYSgxMCwgMTE5LCAyMTMsIDAuMikgNTAlLFxyXG4gIC8vICAgcmdiYSg4MywgMjE3LCAyMjQsIDAuNSkgODAlLFxyXG4gIC8vICAgcmdiYSgxMzUsIDE4OCwgMjM0LCAxKSAxMDAlXHJcbiAgLy8gKTtcclxuICAvLyBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudChcclxuICAvLyAgIDEwLjMxNjQ2NzY4NDMxMDY1MmRlZyxcclxuICAvLyAgIHJnYmEoMTUsIDI2LCAzNSwgMSkgMCxcclxuICAvLyAgIHJnYmEoMTE1LCAxNzcsIDIzMSwgMC41KSAyNCUsXHJcbiAgLy8gICByZ2JhKDEwLCAxMTksIDIxMywgMC4yKSA1MCUsXHJcbiAgLy8gICByZ2JhKDgzLCAyMTcsIDIyNCwgMC41KSA4MCUsXHJcbiAgLy8gICByZ2JhKDEzNSwgMTg4LCAyMzQsIDEpIDEwMCVcclxuICAvLyApO1xyXG4gIC8vIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChcclxuICAvLyAgIDEwLjMxNjQ2NzY4NDMxMDY1MmRlZyxcclxuICAvLyAgIHJnYmEoMTUsIDI2LCAzNSwgMSkgMCxcclxuICAvLyAgIHJnYmEoMTE1LCAxNzcsIDIzMSwgMC41KSAyNCUsXHJcbiAgLy8gICByZ2JhKDEwLCAxMTksIDIxMywgMC4yKSA1MCUsXHJcbiAgLy8gICByZ2JhKDgzLCAyMTcsIDIyNCwgMC41KSA4MCUsXHJcbiAgLy8gICByZ2JhKDEzNSwgMTg4LCAyMzQsIDEpIDEwMCVcclxuICAvLyApO1xyXG4gIC8vIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XHJcbiAgLy8gLXdlYmtpdC1iYWNrZ3JvdW5kLW9yaWdpbjogcGFkZGluZy1ib3g7XHJcbiAgLy8gYmFja2dyb3VuZC1vcmlnaW46IHBhZGRpbmctYm94O1xyXG4gIC8vIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiBib3JkZXItYm94O1xyXG4gIC8vIGJhY2tncm91bmQtY2xpcDogYm9yZGVyLWJveDtcclxuICAvLyAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogYXV0byBhdXRvO1xyXG4gIC8vIGJhY2tncm91bmQtc2l6ZTogYXV0byBhdXRvO1xyXG5cclxuICBoZWlnaHQ6IDUwdmg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLy8gb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgb2JqZWN0LWZpdDogc2NhbGUtZG93bjtcclxufVxyXG5cclxuLmNvbXBsZW1lbnRvLWltYWdlbi0zNjAge1xyXG4gIC8vIC13ZWJraXQtYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgLy8gLW1vei1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcclxuICAvLyBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcclxuICAvLyBvcGFjaXR5OiAwLjk1O1xyXG4gIC8vIC8vIHBhZGRpbmc6IDI4MHB4O1xyXG4gIC8vIGJvcmRlcjogbm9uZTtcclxuICAvLyAvLyBmb250OiBub3JtYWwgMTZweC8xIFwiVGltZXMgTmV3IFJvbWFuXCIsIFRpbWVzLCBzZXJpZjtcclxuICAvLyBjb2xvcjogYmxhY2s7XHJcbiAgLy8gLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgLy8gdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgLy8gYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoXHJcbiAgLy8gICA3OS42ODM1MzIzMTU2ODkzNWRlZyxcclxuICAvLyAgIHJnYmEoMTUsIDI2LCAzNSwgMSkgMCxcclxuICAvLyAgIHJnYmEoMTE1LCAxNzcsIDIzMSwgMC41KSAyNCUsXHJcbiAgLy8gICByZ2JhKDEwLCAxMTksIDIxMywgMC4yKSA1MCUsXHJcbiAgLy8gICByZ2JhKDgzLCAyMTcsIDIyNCwgMC41KSA4MCUsXHJcbiAgLy8gICByZ2JhKDEzNSwgMTg4LCAyMzQsIDEpIDEwMCVcclxuICAvLyApO1xyXG4gIC8vIGJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KFxyXG4gIC8vICAgMTAuMzE2NDY3Njg0MzEwNjUyZGVnLFxyXG4gIC8vICAgcmdiYSgxNSwgMjYsIDM1LCAxKSAwLFxyXG4gIC8vICAgcmdiYSgxMTUsIDE3NywgMjMxLCAwLjUpIDI0JSxcclxuICAvLyAgIHJnYmEoMTAsIDExOSwgMjEzLCAwLjIpIDUwJSxcclxuICAvLyAgIHJnYmEoODMsIDIxNywgMjI0LCAwLjUpIDgwJSxcclxuICAvLyAgIHJnYmEoMTM1LCAxODgsIDIzNCwgMSkgMTAwJVxyXG4gIC8vICk7XHJcbiAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KFxyXG4gIC8vICAgMTAuMzE2NDY3Njg0MzEwNjUyZGVnLFxyXG4gIC8vICAgcmdiYSgxNSwgMjYsIDM1LCAxKSAwLFxyXG4gIC8vICAgcmdiYSgxMTUsIDE3NywgMjMxLCAwLjUpIDI0JSxcclxuICAvLyAgIHJnYmEoMTAsIDExOSwgMjEzLCAwLjIpIDUwJSxcclxuICAvLyAgIHJnYmEoODMsIDIxNywgMjI0LCAwLjUpIDgwJSxcclxuICAvLyAgIHJnYmEoMTM1LCAxODgsIDIzNCwgMSkgMTAwJVxyXG4gIC8vICk7XHJcbiAgLy8gYmFja2dyb3VuZC1wb3NpdGlvbjogNTAlIDUwJTtcclxuICAvLyAtd2Via2l0LWJhY2tncm91bmQtb3JpZ2luOiBwYWRkaW5nLWJveDtcclxuICAvLyBiYWNrZ3JvdW5kLW9yaWdpbjogcGFkZGluZy1ib3g7XHJcbiAgLy8gLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IGJvcmRlci1ib3g7XHJcbiAgLy8gYmFja2dyb3VuZC1jbGlwOiBib3JkZXItYm94O1xyXG4gIC8vIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBhdXRvIGF1dG87XHJcbiAgLy8gYmFja2dyb3VuZC1zaXplOiBhdXRvIGF1dG87XHJcblxyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICAvLyBvYmplY3QtZml0OiBjb3ZlcjtcclxuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xyXG4gIG1hcmdpbi10b3A6IDQ5cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcblxyXG4uZGl2QnRue1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyOyBhbGlnbi1pdGVtczogY2VudGVyOyBjb2xvcjogd2hpdGU7IGZvbnQtc2l6ZTogMzBweDtcclxufVxyXG5cclxuXHJcbi5zbGlkZTM2MHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmF5O2JvcmRlci1yYWRpdXM6IDUwcHg7bWFyZ2luLXRvcDogMHB4O21hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuLnNsaWRlMzYwOmFjdGl2ZSB7XHJcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgb3BhY2l0eTogMC41O1xyXG4gIH0iLCJpb24taXRlbSB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG5cbi5jb21wbGVtZW50by1pbWFnZW4ge1xuICBoZWlnaHQ6IDUwdmg7XG4gIHdpZHRoOiAxMDAlO1xuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xufVxuXG4uY29tcGxlbWVudG8taW1hZ2VuLTM2MCB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XG4gIG1hcmdpbi10b3A6IDQ5cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuLmRpdkJ0biB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLnNsaWRlMzYwIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JheTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi5zbGlkZTM2MDphY3RpdmUge1xuICBvcGFjaXR5OiAwLjU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/house-detail/house-detail.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/house-detail/house-detail.page.ts ***!
  \*********************************************************/
/*! exports provided: HouseDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseDetailPage", function() { return HouseDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/mysevice.service */ "./src/app/services/mysevice.service.ts");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/variables.service */ "./src/app/services/variables.service.ts");


// nuevos





let HouseDetailPage = class HouseDetailPage {
    constructor(router, alertCtrl, st, mys, myv, loadingCtrl) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.st = st;
        this.mys = mys;
        this.myv = myv;
        this.loadingCtrl = loadingCtrl;
        this.prueba = 'varhbthathasrtjjts';
        this.titulox = 'mititulo';
        this.urlimage = this.mys.UrlBase + '/Images/Inmuebles/';
        this.options2d = { slidesPerView: 1 };
        this.options360 = { slidesPerView: 3.5 };
        this.array360url = [
            '../../../assets/360/1.jpg',
            '../../../assets/360/2.jpg',
            '../../../assets/360/3.jpg',
            '../../../assets/360/4.jpg',
            '../../../assets/360/5.jpg'
        ];
    }
    ngOnInit() {
        if (this.myv.isSetInmueble()) {
            this.inmueble = this.myv.getInmueble();
        }
        else {
            this.router.navigateByUrl('home');
        }
        // definir cuantas slides colocar en 360
        switch (this.array360url.length) {
            case 0:
                this.options360.slidesPerView = 0;
                break;
            case 1:
                this.options360.slidesPerView = 1;
                break;
            case 2:
                this.options360.slidesPerView = 2;
                break;
            case 3:
                this.options360.slidesPerView = 3;
                break;
            default:
                this.options360.slidesPerView = 3.5;
                break;
        }
    }
    // @ViewChild('') slides: IonSlides;
    planificarvisita() {
        if (this.myv.isUserSigned()) {
            this.router.navigateByUrl('create-visit');
        }
        else {
            this.router.navigateByUrl('login');
        }
        // this.st.get('user').then(valor => {
        //   if (valor) {
        //     this.router.navigateByUrl('create-visit');
        //   } else {
        //     this.router.navigateByUrl('login');
        //   }
        // });
    } // fin planificarvisita
    ionViewWillEnter() { }
    ionViewDidEnter() { }
    ionViewDidLeave() {
        this.myv.setRemoveInmueble();
    }
    whatsapp() {
        window.open('whatsapp://send?phone=584242332373?text=Me%20gustaría%20saber%20el%20precio%20del%20coche');
    }
    tourVirtual() {
        console.log('Éste inmueble tiene tour Virtual');
        this.view360url('../../../assets/360/a.jpg');
    }
    view360url(url) {
        this.presentLoading();
        this.mys.view360url = url;
        this.router.navigateByUrl('/view360');
    }
    presentLoading() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando Imagen 360°',
                duration: 500
            });
            yield loading.present();
        });
    }
}; // fin clase
HouseDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__["MyseviceService"] },
    { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
HouseDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-house-detail',
        template: __webpack_require__(/*! raw-loader!./house-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/house-detail/house-detail.page.html"),
        styles: [__webpack_require__(/*! ./house-detail.page.scss */ "./src/app/pages/house-detail/house-detail.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _services_mysevice_service__WEBPACK_IMPORTED_MODULE_5__["MyseviceService"],
        _services_variables_service__WEBPACK_IMPORTED_MODULE_6__["VariablesService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
], HouseDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-house-detail-house-detail-module-es2015.js.map