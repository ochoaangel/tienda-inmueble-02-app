(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\" style=\"color: white;\">\n      <!-- <ion-menu-button> </ion-menu-button> -->\n      <ion-icon slot=\"icon-only\" name=\"arrow-back\" *ngIf=\"nfiltro>0\" (click)=\"regresarFiltro()\"></ion-icon>\n    </ion-buttons>\n    <ion-title style=\"color: white; text-align: center; padding-left:  60px;\" *ngIf=\"nfiltro==0 \">!Bienvenidos!\n    </ion-title>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\" *ngIf=\"nfiltro==1 \">Estado</ion-title>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\" *ngIf=\"nfiltro==2 \">Municipio</ion-title>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\" *ngIf=\"nfiltro==3 \">Uso</ion-title>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\" *ngIf=\"nfiltro==4 \">{{this.filtros.f4}}\n    </ion-title>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\" *ngIf=\"nfiltro==5 \">Resultados\n    </ion-title>\n    <ion-title>\n      <!-- <ion-img src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\"></ion-img> -->\n    </ion-title>\n    <ion-buttons slot=\"end\" style=\"color: white;\">\n      <ion-menu-button> </ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content style=\"--background:var(--ion-color-light)\">\n  <img src=\"../../assets/myimages/inicial.jpg\" style=\"width: 100%; height: 30%;\n    object-fit: cover;\" *ngIf=\"nfiltro !== 5\" />\n\n  <!-- input busqueda -->\n  <ion-grid fixed style=\"width: 100%; margin-top: 1%;\">\n    <ion-row style=\"width: 100%;\">\n      <ion-col size=\"12\" style=\"width: 100%;\" class=\"ion-no-padding\">\n        <div style=\"text-align: center; width: 100%;\">\n          <div>\n            \n            <div style=\"display: inline-block; width: 10%;\"></div>\n\n            <div style=\"display: inline-block; width: 10%; font-size: 200%; vertical-align: middle;\">\n              <ion-icon name=\"md-search\" style=\"color: grey; margin-top: 4px;\"></ion-icon>\n            </div>\n\n            <div style=\"display: inline-block; width: 70%;\">\n              <ion-input class=\"inputBuscador\" placeholder=\"\"></ion-input>\n            </div>\n\n            <div style=\"display: inline-block; width: 10%;\"></div>\n\n\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- FILTRO 1 -->\n  <div *ngIf=\"!filtros.f1 && !filtros.f2 && !filtros.f3 && !filtros.f4 && !filtros.f5\">\n    <div style=\"text-align: center; width: 100%; margin-top: 1%;\">\n      <ion-text color=\"primary\">Quieres buscar algo?</ion-text>\n    </div>\n\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"6\" style=\"text-align: center;\">\n          <ion-img src=\"../../assets/myimages/btnVenta.png\" style=\"height:\n            80px;\" (click)=\"setFiltro1('venta')\"></ion-img>\n          <ion-text color=\"tertiary\">Venta</ion-text>\n        </ion-col>\n        <ion-col size=\"6\" style=\"text-align: center;border-left:2px solid;\n          color: var(--ion-color-tertiary);\">\n          <ion-img src=\"../../assets/myimages/btnAlquiler.png\" style=\"height:\n            80px;\" (click)=\"setFiltro1('alquiler')\"></ion-img>\n          <ion-text color=\"tertiary\">Alquiler</ion-text>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- FILTRO 2 -->\n  <div *ngIf=\"!filtros.f2 && !filtros.f3 && !filtros.f4 && !filtros.f5 && filtros.f1\">\n    <div style=\"text-align: center; width: 100%; margin-top: 1%;\">\n      <ion-text color=\"primary\">Elija el Estado de su preferencia</ion-text>\n    </div>\n\n    <ion-grid fixed>\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Miranda</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">5</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Falcón</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">11</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Lara</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Zulia</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">6</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Bolívar</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">10</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Anzoategui</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">1</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Barinas</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">5</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Táchira</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">13</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro2('estado')\">\n            <ion-label>Guarico</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n\n\n\n  </div>\n\n  <!-- FILTRO 3 -->\n  <div *ngIf=\"!filtros.f3 && !filtros.f4 && !filtros.f5 && filtros.f1 && filtros.f2\">\n    <div style=\"text-align: center; width: 100%; margin-top: 1%;\">\n      <ion-text color=\"primary\">Elija el Municipio de su preferencia</ion-text>\n    </div>\n\n    <!-- <ion-list> -->\n    <ion-grid fixed>\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Baruta</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">7</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Chacao</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">10</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Hatillo</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">1</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Libertador</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">5</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Sucre</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">13</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro3('municipio')\">\n            <ion-label>Tomas Lander</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n  </div>\n\n  <!-- FILTRO 4 -->\n  <div *ngIf=\"!filtros.f4 && !filtros.f5 && filtros.f1 && filtros.f2 && filtros.f3\">\n    <div style=\"text-align: center; width: 100%; margin-top: 1%;\">\n      <ion-text color=\"primary\">Elija el Uso para inmueble</ion-text>\n    </div>\n\n    <ion-grid fixed>\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro4('Residencial')\">\n            <ion-label>Residencial</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">15</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro4('Comercial')\">\n            <ion-label>Comercial</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">3</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro4('Industrial')\">\n            <ion-label>Industrial</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- FILTRO 5 -->\n  <div *ngIf=\"!filtros.f5 && filtros.f1 && filtros.f2 && filtros.f3 && filtros.f4\">\n    <div style=\"text-align: center; width: 100%; margin-top: 1%;\">\n      <ion-text color=\"primary\">Elija el Tipo de inmueble</ion-text>\n    </div>\n\n    <ion-grid fixed *ngIf=\"filtros.f4 == 'Comercial'\">\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('local')\">\n            <ion-label>Local</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">15</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('oficina')\">\n            <ion-label>Oficina</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">3</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('edificio')\">\n            <ion-label>Edificio</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('casa')\">\n            <ion-label>Casa</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('terreno')\">\n            <ion-label>Terreno</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n\n      </ion-row>\n    </ion-grid>\n\n    <!-- Caso Industrial -->\n    <ion-grid fixed *ngIf=\"filtros.f4 == 'Industrial'\">\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('galpon')\">\n            <ion-label>Galpon</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">15</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('planta')\">\n            <ion-label>Planta</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">3</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('edificio')\">\n            <ion-label>Edificio</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('terreno')\">\n            <ion-label>Terreno</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n    <!-- Caso Residencial -->\n    <ion-grid fixed *ngIf=\"filtros.f4 == 'Residencial'\">\n      <ion-row justify-content-center align-items-center>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('casa')\">\n            <ion-label>Casa</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">15</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('apartamento')\">\n            <ion-label>Apartamento</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">3</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('town house')\">\n            <ion-label>Town House</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('pent house')\">\n            <ion-label>Pent House</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\n          <ion-item (click)=\"setFiltro5('planta baja')\">\n            <ion-label>Planta Baja</ion-label>\n            <ion-checkbox slot=\"start\" hidden></ion-checkbox>\n            <ion-badge style=\"color:var(--ion-color-tertiary); background-color:linen;\" slot=\"end\">8</ion-badge>\n          </ion-item>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n\n    <!-- </ion-list> -->\n  </div>\n\n\n  <!-- resultados del filtro f5 -->\n  <div *ngIf=\"filtros.f5\">\n\n    <ion-grid class=\"ion-no-padding\">\n      <ion-row justify-content-center align-items-center>\n        <ion-col size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\" *ngFor=\"let inmueble of inmuebles\">\n          <ion-card class=\"welcome-card\">\n            <img src=\"{{ urlimage + inmueble.pics[0] }}\" class=\"complemento-imagen\"\n              (click)=\"detalleinmueble(inmueble.id)\" />\n            <ion-grid class=\"footerDelIionCard\">\n              <ion-row>\n                <ion-col size=\"10\" (click)=\"detalleinmueble(inmueble.id)\">\n                  <ion-card-title>\n                    <ion-text color=\"secondary\">\n                      <strong style=\"font-size: 12px;\">{{ inmueble.name }}</strong>\n                      <!-- <strong>{{ inmueble.name.substring(0,6) }}</strong> -->\n                      <!-- <strong>{{ (inmueble.name.length < 20) ? inmueble.name :inmueble.name.substring(0,20)+'...'  }}</strong> -->\n                    </ion-text>\n                  </ion-card-title>\n                </ion-col>\n                <ion-col>\n                  <div class=\"inmueble-more\">\n                    <ion-button fill=\"clear\" (click)=\"alanzarMenu()\" style=\"height:\n                      100%\">\n                      <ion-icon slot=\"icon-only\" name=\"more\" color=\"secondary\"></ion-icon>\n                    </ion-button>\n                  </div>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card>\n        </ion-col>\n\n      </ion-row>\n    </ion-grid>\n\n\n    <!-- ooooooooooooooooooooooooooooooooFIN BUCLEoooooooooooooooooooooooooooooooo -->\n  </div>\n\n\n\n</ion-content>\n\n\n\n\n<!-- caso no registrado -->\n<!-- <ion-button shape=\"round\" size=\"small\" color=\"tertiary\" routerLink=\"/login\" *ngIf=\"!this.userSigned\"\n    style=\"text-transform: none;\" routerLink=\"/login\">\n    <ion-text color=\"light\">\n    Inicio de sesión \n  </ion-text>\n</ion-button> -->\n\n<!-- Dando bienvenida -->\n<!-- <ion-row class=\"ion-justify-content-center\">\n    <ion-label *ngIf=\"this.userSigned\">Bienvenido {{ this.user.email }}</ion-label>\n    <ion-button expand=\"full\" *ngIf=\"this.userSigned\" (click)=\"cerrarsesion()\">Cierra\n      sesión</ion-button>\n  </ion-row> -->\n<!-- <a href=\"#\" class=\"myButton\">Entrar</a> -->\n\n<!-- <ion-img src=\"../../assets/myimages/inicial.jpg\" ></ion-img> -->\n\n<!-- <div style=\"text-align: center; width: 100%; margin-top: 30px;\">\n    <ion-grid fixed style=\"margin-top: 20px;\">\n      <ion-row justify-content-center align-items-center style='height: 100%'>\n\n        <ion-col size=\"1\"></ion-col>\n        <ion-col size=\"1\" style=\"font-size: 200%; line-height: 20px; color: gray;\">\n          <ion-icon name=\"md-search\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"9\" class=\"ion-text-left\"> <input class=\"inputBuscador\" placeholder=\"\" /></ion-col>\n        <ion-col size=\"1\"></ion-col>\n\n      </ion-row>\n    </ion-grid>\n  </div> -->\n<!-- <div style=\"text-align: center; width: 100%; margin-top: 30px;\">\n    <input class=\"inputBuscador\" placeholder=\"\" />\n  </div> -->\n<!-- ooooooooooooooooooooooooooooooooBUCLEoooooooooooooooooooooooooooooooo -->\n\n<!-- <ion-footer>\n  <ion-toolbar>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon slot=\"icon-only\" name=\"finger-print\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n    <ion-title>Footer</ion-title>\n\n    <ion-buttons slot=\"primary\">\n      <ion-button>\n        <ion-icon slot=\"icon-only\" name=\"more\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-footer> -->\n\n<!-- <ion-button shape=\"round\" color=\"success\" style=\"text-transform: none;\" (click)=\"pruebax()\">\n  <ion-text color=\"light\">\n    Prueba\n  </ion-text>\n</ion-button> -->\n\n<!-- <div >\n  <div style=\"width:18%; display:inline-table\">Precio</div>\n  <div style=\"width:50%; display:inline-table\">Valor</div>\n</div> -->\n\n<!-- <ion-img src=\"../../assets/myimages/gif.gif\" ></ion-img> -->\n\n<!-- <ion-button expand=\"full\" routerLink=\"/login\" *ngIf=\"!this.userSigned\">\n  Bienvenido... Inicia Sesión..</ion-button\n> -->\n<!-- <a href=\"whatsapp://send?phone=584242332373?text=Me%20gustaría%20saber%20el%20precio%20del%20coche\">Link WhatsApp</a> -->\n<!-- \n  <ion-img class=\"animated infinite bounce delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flash delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite pulse delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rubberBand delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite shake delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite headShake delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite swing delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite tada delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite wobble delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite jello delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite bounceOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInDownBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInLeftBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInRightBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeInUpBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutDownBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutLeftBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutRightBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite fadeOutUpBig delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipInX delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipInY delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipOutX delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite flipOutY delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite lightSpeedIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite lightSpeedOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInDownLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInDownRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInUpLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateInUpRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutDownLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutDownRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutUpLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rotateOutUpRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite hinge delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite jackInTheBox delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rollIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite rollOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomIn delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOut delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite zoomOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideInUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutDown delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutLeft delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutRight delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite slideOutUp delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img>\n  <ion-img class=\"animated infinite heartBeat delay-5s\" src=\"../../assets/myimages/header.jpg\" style=\"height: 25px;\" ></ion-img> -->\n<!-- https://techart.pw/__cpi.php?s=UkQ2YXlSaWJuc3ZoeGR2dG04WW9Ldkt5c0JwSnpnRWFTYjBHZGdPdXZ5K1BZK0RwajhxNGs3eG1LeHQ2dEZPNVpYYXlEeWtLWUFDVlp1M2t4ZXNlRG5SVHJjNGgwYytvOVVaVVZwQ2dBMzA9&r=aHR0cHM6Ly90ZWNoYXJ0LnB3L3Nob3cvP209NXBXQ2pxZGlKZW0mX19jcG89YUhSMGNITTZMeTl0ZVM1dFlYUjBaWEp3YjNKMExtTnZiUQ==&__cpo=1 -->"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: "",
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ]),
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.inmueble-more {\n  font-size: 25px !important;\n  position: absolute;\n  right: -10px;\n  top: -10px;\n  height: 100%;\n}\n\n.myButton {\n  background: -webkit-gradient(linear, left top, left bottom, color-stop(5%, #ffffff), to(#fcfcfc));\n  background: linear-gradient(to bottom, #ffffff 5%, #fcfcfc 100%);\n  background-color: #ffffff;\n  border-radius: 42px;\n  display: inline-block;\n  cursor: pointer;\n  color: #3fab4f;\n  font-family: Arial;\n  font-size: 17px;\n  padding: 13px 76px;\n  text-decoration: none;\n}\n\n.inputBuscador {\n  display: inline-block;\n  box-sizing: content-box;\n  padding: 10px 20px;\n  border: none;\n  border-radius: 20px;\n  color: grey;\n  text-overflow: clip;\n  background: #e8e8e8;\n  text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.66);\n  width: -webkit-fill-available;\n  height: 30px;\n  --padding-start: 0px !important;\n}\n\nion-img:active {\n  opacity: 0.5;\n}\n\n.complemento-imagen {\n  box-sizing: content-box;\n  opacity: 0.95;\n  border: none;\n  color: black;\n  text-overflow: ellipsis;\n  background: linear-gradient(10.3164676843deg, #0f1a23 0, rgba(115, 177, 231, 0.5) 24%, rgba(10, 119, 213, 0.2) 50%, rgba(83, 217, 224, 0.5) 80%, #87bcea 100%);\n  background-position: 50% 50%;\n  background-origin: padding-box;\n  -webkit-background-clip: border-box;\n  background-clip: border-box;\n  background-size: auto auto;\n  height: 150px;\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.footerDelIionCard {\n  height: 40px;\n}\n\nion-item {\n  --background:var(--ion-color-light) ;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXGFwcFxcRG9jdW1lbnRzXFwtLW1pc1Byb3llY3Rvc1xcdGllbmRhLWlubXVlYmxlLTAyLWFwcC9zcmNcXGFwcFxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSwwQkFBQTtFQUNBLGtCQUFBO0VBRUEsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FDQUY7O0FES0E7RUFDQyxpR0FBQTtFQUFBLGdFQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FDRkQ7O0FEYUE7RUFDRSxxQkFBQTtFQUdBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBRUEsbUJBQUE7RUFHQSxXQUFBO0VBRUEsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdEQUFBO0VBRUEsNkJBQUE7RUFFQSxZQUFBO0VBQ0EsK0JBQUE7QUNkRjs7QURrQkE7RUFFQSxZQUFBO0FDaEJBOztBRG1CQTtFQUdFLHVCQUFBO0VBQ0EsYUFBQTtFQUVBLFlBQUE7RUFFQSxZQUFBO0VBRUEsdUJBQUE7RUFHQSw4SkFBQTtFQUNBLDRCQUFBO0VBRUEsOEJBQUE7RUFDQSxtQ0FBQTtFQUNBLDJCQUFBO0VBRUEsMEJBQUE7RUFFRCxhQUFBO0VBQWUsV0FBQTtFQUFhLG9CQUFBO0tBQUEsaUJBQUE7QUNqQjdCOztBRHNCQTtFQUNFLFlBQUE7QUNuQkY7O0FEc0JBO0VBQ0Usb0NBQUE7QUNuQkYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uaW5tdWVibGUtbW9yZSB7XG4gIGZvbnQtc2l6ZTogMjVweCAhaW1wb3J0YW50O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcmlnaHQ6IC0xMHB4O1xuICB0b3A6IC0xMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG5cbiAgLy8gYm90dG9tOiAtNXB4O1xufVxuXG4ubXlCdXR0b24ge1xuXHRiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byBib3R0b20sICNmZmZmZmYgNSUsICNmY2ZjZmMgMTAwJSk7XG5cdGJhY2tncm91bmQtY29sb3I6I2ZmZmZmZjtcblx0Ym9yZGVyLXJhZGl1czo0MnB4O1xuXHRkaXNwbGF5OmlubGluZS1ibG9jaztcblx0Y3Vyc29yOnBvaW50ZXI7XG5cdGNvbG9yOiMzZmFiNGY7XG5cdGZvbnQtZmFtaWx5OkFyaWFsO1xuXHRmb250LXNpemU6MTdweDtcblx0cGFkZGluZzoxM3B4IDc2cHg7XG5cdHRleHQtZGVjb3JhdGlvbjpub25lO1xufVxuLy8gLm15QnV0dG9uOmhvdmVyIHtcbi8vIFx0YmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCAjZmNmY2ZjIDUlLCAjZmZmZmZmIDEwMCUpO1xuLy8gXHRiYWNrZ3JvdW5kLWNvbG9yOiNmY2ZjZmM7XG4vLyB9XG4vLyAubXlCdXR0b246YWN0aXZlIHtcbi8vIFx0cG9zaXRpb246cmVsYXRpdmU7XG4vLyBcdHRvcDoxcHg7XG4vLyB9XG5cbi5pbnB1dEJ1c2NhZG9yIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICAtbW96LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgLy8gZm9udDogbm9ybWFsIDE2cHgvbm9ybWFsIFwiVGltZXMgTmV3IFJvbWFuXCIsIFRpbWVzLCBzZXJpZjtcbiAgLy8gY29sb3I6IHJnYmEoMCwxNDIsMTk4LDEpO1xuICBjb2xvcjogZ3JleTtcbiAgLW8tdGV4dC1vdmVyZmxvdzogY2xpcDtcbiAgdGV4dC1vdmVyZmxvdzogY2xpcDtcbiAgYmFja2dyb3VuZDogcmdiYSgyMzIsMjMyLDIzMiwxKTtcbiAgdGV4dC1zaGFkb3c6IDFweCAxcHggMCByZ2JhKDI1NSwyNTUsMjU1LDAuNjYpIDtcbiAgLy8gd2lkdGg6IDcwJTtcbiAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG4gIFxuICBoZWlnaHQ6IDMwcHg7XG4gIC0tcGFkZGluZy1zdGFydDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuaW9uLWltZzphY3RpdmUge1xuLy8gYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG5vcGFjaXR5OiAwLjU7XG59XG5cbi5jb21wbGVtZW50by1pbWFnZW4ge1xuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICAtbW96LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgb3BhY2l0eTogMC45NTtcbiAgLy8gcGFkZGluZzogMjgwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgLy8gZm9udDogbm9ybWFsIDE2cHgvMSBcIlRpbWVzIE5ldyBSb21hblwiLCBUaW1lcywgc2VyaWY7XG4gIGNvbG9yOiBibGFjaztcbiAgLW8tdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCg3OS42ODM1MzIzMTU2ODkzNWRlZywgcmdiYSgxNSwyNiwzNSwxKSAwLCByZ2JhKDExNSwxNzcsMjMxLDAuNSkgMjQlLCByZ2JhKDEwLDExOSwyMTMsMC4yKSA1MCUsIHJnYmEoODMsMjE3LDIyNCwwLjUpIDgwJSwgcmdiYSgxMzUsMTg4LDIzNCwxKSAxMDAlKTtcbiAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoMTAuMzE2NDY3Njg0MzEwNjUyZGVnLCByZ2JhKDE1LDI2LDM1LDEpIDAsIHJnYmEoMTE1LDE3NywyMzEsMC41KSAyNCUsIHJnYmEoMTAsMTE5LDIxMywwLjIpIDUwJSwgcmdiYSg4MywyMTcsMjI0LDAuNSkgODAlLCByZ2JhKDEzNSwxODgsMjM0LDEpIDEwMCUpO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTAuMzE2NDY3Njg0MzEwNjUyZGVnLCByZ2JhKDE1LDI2LDM1LDEpIDAsIHJnYmEoMTE1LDE3NywyMzEsMC41KSAyNCUsIHJnYmEoMTAsMTE5LDIxMywwLjIpIDUwJSwgcmdiYSg4MywyMTcsMjI0LDAuNSkgODAlLCByZ2JhKDEzNSwxODgsMjM0LDEpIDEwMCUpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xuICAtd2Via2l0LWJhY2tncm91bmQtb3JpZ2luOiBwYWRkaW5nLWJveDtcbiAgYmFja2dyb3VuZC1vcmlnaW46IHBhZGRpbmctYm94O1xuICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogYm9yZGVyLWJveDtcbiAgYmFja2dyb3VuZC1jbGlwOiBib3JkZXItYm94O1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogYXV0byBhdXRvO1xuICBiYWNrZ3JvdW5kLXNpemU6IGF1dG8gYXV0bztcblxuIGhlaWdodDogMTUwcHg7IHdpZHRoOiAxMDAlOyBvYmplY3QtZml0OiBjb3ZlcjtcblxuXG59XG5cbi5mb290ZXJEZWxJaW9uQ2FyZCB7XG4gIGhlaWdodDogNDBweDsgXG59IFxuXG5pb24taXRlbSB7XG4gIC0tYmFja2dyb3VuZDp2YXIoLS1pb24tY29sb3ItbGlnaHQpXG59XG5cbi8vIC5pbnB1dEJ1c2NhZG9yIHtcbi8vICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmMTQ1M2QpO1xuLy8gfSIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLmlubXVlYmxlLW1vcmUge1xuICBmb250LXNpemU6IDI1cHggIWltcG9ydGFudDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogLTEwcHg7XG4gIHRvcDogLTEwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm15QnV0dG9uIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2ZmZmZmZiA1JSwgI2ZjZmNmYyAxMDAlKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogNDJweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGNvbG9yOiAjM2ZhYjRmO1xuICBmb250LWZhbWlseTogQXJpYWw7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgcGFkZGluZzogMTNweCA3NnB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5pbnB1dEJ1c2NhZG9yIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICAtbW96LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgY29sb3I6IGdyZXk7XG4gIC1vLXRleHQtb3ZlcmZsb3c6IGNsaXA7XG4gIHRleHQtb3ZlcmZsb3c6IGNsaXA7XG4gIGJhY2tncm91bmQ6ICNlOGU4ZTg7XG4gIHRleHQtc2hhZG93OiAxcHggMXB4IDAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjY2KTtcbiAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG4gIGhlaWdodDogMzBweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHggIWltcG9ydGFudDtcbn1cblxuaW9uLWltZzphY3RpdmUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbi5jb21wbGVtZW50by1pbWFnZW4ge1xuICAtd2Via2l0LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICAtbW96LWJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgb3BhY2l0eTogMC45NTtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogYmxhY2s7XG4gIC1vLXRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoNzkuNjgzNTMyMzE1N2RlZywgIzBmMWEyMyAwLCByZ2JhKDExNSwgMTc3LCAyMzEsIDAuNSkgMjQlLCByZ2JhKDEwLCAxMTksIDIxMywgMC4yKSA1MCUsIHJnYmEoODMsIDIxNywgMjI0LCAwLjUpIDgwJSwgIzg3YmNlYSAxMDAlKTtcbiAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoMTAuMzE2NDY3Njg0M2RlZywgIzBmMWEyMyAwLCByZ2JhKDExNSwgMTc3LCAyMzEsIDAuNSkgMjQlLCByZ2JhKDEwLCAxMTksIDIxMywgMC4yKSA1MCUsIHJnYmEoODMsIDIxNywgMjI0LCAwLjUpIDgwJSwgIzg3YmNlYSAxMDAlKTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEwLjMxNjQ2NzY4NDNkZWcsICMwZjFhMjMgMCwgcmdiYSgxMTUsIDE3NywgMjMxLCAwLjUpIDI0JSwgcmdiYSgxMCwgMTE5LCAyMTMsIDAuMikgNTAlLCByZ2JhKDgzLCAyMTcsIDIyNCwgMC41KSA4MCUsICM4N2JjZWEgMTAwJSk7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDUwJSA1MCU7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1vcmlnaW46IHBhZGRpbmctYm94O1xuICBiYWNrZ3JvdW5kLW9yaWdpbjogcGFkZGluZy1ib3g7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiBib3JkZXItYm94O1xuICBiYWNrZ3JvdW5kLWNsaXA6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBhdXRvIGF1dG87XG4gIGJhY2tncm91bmQtc2l6ZTogYXV0byBhdXRvO1xuICBoZWlnaHQ6IDE1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5mb290ZXJEZWxJaW9uQ2FyZCB7XG4gIGhlaWdodDogNDBweDtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLWJhY2tncm91bmQ6dmFyKC0taW9uLWNvbG9yLWxpZ2h0KSA7XG59Il19 */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/mysevice.service */ "./src/app/services/mysevice.service.ts");
/* harmony import */ var _services_variables_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/variables.service */ "./src/app/services/variables.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");


// nuevos




// import { Storage } from '@ionic/storage';

// import { ThrowStmt } from '@angular/compiler';
// import { ActionSheetController } from '@ionic/angular';*****


var HomePage = /** @class */ (function () {
    function HomePage(router, alertController, mys, // private http: HttpClient
    myv, actionSheetCtrl, loadingCtrl, platform, location, menuController) {
        this.router = router;
        this.alertController = alertController;
        this.mys = mys;
        this.myv = myv;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.location = location;
        this.menuController = menuController;
        this.filtros = { f1: '', f2: '', f3: '', f4: '', f5: '' };
        this.nfiltro = 0;
        this.alertSalir = false;
        this.actionSheetVisible = false;
        this.userSigned = false;
        this.urlimage = this.mys.UrlBase + '/Images/Inmuebles/';
        this.filtros = { f1: '', f2: '', f3: '', f4: '', f5: '' };
    }
    HomePage.prototype.ngOnInit = function () {
        console.log('ngOnInit desde home solo la primera ves...');
        this.initializeBackButtonCustomHandler(); // para capturar boton back
    };
    HomePage.prototype.setFiltro0 = function () {
        this.filtros.f1 = '';
        this.filtros.f2 = '';
        this.filtros.f3 = '';
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 0;
    };
    HomePage.prototype.setFiltro1 = function (valor) {
        this.filtros.f1 = valor;
        this.filtros.f2 = '';
        this.filtros.f3 = '';
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 1;
    };
    HomePage.prototype.setFiltro2 = function (valor) {
        this.filtros.f2 = valor;
        this.filtros.f3 = '';
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 2;
    };
    HomePage.prototype.setFiltro3 = function (valor) {
        this.filtros.f3 = valor;
        this.filtros.f4 = '';
        this.filtros.f5 = '';
        this.nfiltro = 3;
    };
    HomePage.prototype.setFiltro4 = function (valor) {
        this.filtros.f4 = valor;
        this.filtros.f5 = '';
        this.nfiltro = 4;
    };
    HomePage.prototype.setFiltro5 = function (valor) {
        this.filtros.f5 = valor;
        this.nfiltro = 5;
        console.log('Termino los filtros..', this.filtros);
        // this.setFiltro0();
        // console.log('Termino los filtros..', this.filtros);
    };
    HomePage.prototype.regresarFiltro = function () {
        switch (this.nfiltro) {
            case 1:
                this.setFiltro0();
                break;
            case 2:
                this.filtros.f2 = '';
                this.filtros.f3 = '';
                this.filtros.f4 = '';
                this.filtros.f5 = '';
                this.nfiltro = 1;
                break;
            case 3:
                this.filtros.f3 = '';
                this.filtros.f4 = '';
                this.filtros.f5 = '';
                this.nfiltro = 2;
                break;
            case 4:
                this.filtros.f4 = '';
                this.filtros.f5 = '';
                this.nfiltro = 3;
                break;
            case 5:
                this.filtros.f5 = '';
                this.nfiltro = 4;
                break;
            default:
                this.setFiltro0();
                break;
        }
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.inmuebles = this.myv.getInmuebles();
        this.user = this.myv.getUserSigned();
        this.userSigned = this.myv.isUserSigned();
        if (this.userSigned) {
            this.user = this.myv.getUserSigned();
            this.userSigned = true;
        }
        else {
            this.user = '';
            this.userSigned = false;
        }
    };
    HomePage.prototype.detalleinmueble = function (ninmueble) {
        var inmuebles = this.myv.getInmuebleDetailId(ninmueble);
        this.myv.setInmueble(inmuebles);
        this.router.navigateByUrl('house-detail');
    };
    HomePage.prototype.ionViewDidEnter = function () {
        if (this.mys.isLoading) {
            this.mys.loadingDismiss();
        }
    };
    HomePage.prototype.alanzarMenu = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.actionSheetCtrl.create({
                                backdropDismiss: true,
                                buttons: [
                                    {
                                        text: 'Compartir en Redes',
                                        icon: 'share',
                                        cssClass: 'actionSheet-Global-inmueble',
                                        handler: function () {
                                            console.log('Se pulsó en Compartir');
                                        }
                                    },
                                    {
                                        text: 'Añadir a Favorito',
                                        icon: 'star',
                                        cssClass: 'actionSheet-Global-inmueble',
                                        handler: function () {
                                            console.log('Se pulsó en Añadir a Favorito');
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        _a.actionSheet = _b.sent();
                        this.actionSheetVisible = true;
                        this.actionSheet.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.initializeBackButtonCustomHandler = function () {
        var _this = this;
        this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(999999, function () {
            _this.menuController.isOpen().then(function (ret) {
                if (ret) {
                    // si esta abierto el menu lo cierro
                    _this.menuController.close('mymenu');
                }
                else {
                    // si NO esta abierto el menu
                    if (_this.actionSheetVisible) {
                        _this.actionSheetVisible = false;
                        _this.actionSheet.dismiss();
                    }
                    else {
                        if (_this.router.url === '/home') {
                            if (_this.nfiltro === 0) {
                                if (!_this.alertSalir) {
                                    _this.alertSalir = !_this.alertSalir;
                                    _this.presentAlertConfirm();
                                }
                            }
                            else {
                                switch (_this.nfiltro) {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                        _this.regresarFiltro();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else {
                            _this.location.back();
                        }
                    }
                }
            });
        });
    };
    HomePage.prototype.presentAlertConfirm = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Desea Cerrar la Aplicación?',
                            buttons: [
                                {
                                    text: 'NO',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Volviendo a la App  ' + _this.constructor.name);
                                        _this.alertSalir = !_this.alertSalir;
                                    }
                                },
                                {
                                    text: 'SI',
                                    handler: function () {
                                        console.log('Cerrando la App');
                                        _this.alertSalir = !_this.alertSalir;
                                        navigator['app'].exitApp();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.OnDestroy = function () {
        this.unsubscribeBackEvent && this.unsubscribeBackEvent();
        console.log('fffffffffffffffffffhhhhhhhhhhhhhhhhhh');
    };
    HomePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__["MyseviceService"] },
        { type: _services_variables_service__WEBPACK_IMPORTED_MODULE_5__["VariablesService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _services_mysevice_service__WEBPACK_IMPORTED_MODULE_4__["MyseviceService"],
            _services_variables_service__WEBPACK_IMPORTED_MODULE_5__["VariablesService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map