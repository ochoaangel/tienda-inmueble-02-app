(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-view360-view360-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/view360/view360.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/view360/view360.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<a-scene >\n    <a-entity touch-controls ></a-entity>\n    <a-assets>\n        <img id=\"panorama\" [src]=\"view360url\" crossorigin />\n    </a-assets>\n    <a-sky src=\"#panorama\" rotation=\"0 -90 0\"></a-sky>\n</a-scene>"

/***/ }),

/***/ "./src/app/pages/view360/view360.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/view360/view360.module.ts ***!
  \*************************************************/
/*! exports provided: View360PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View360PageModule", function() { return View360PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _view360_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view360.page */ "./src/app/pages/view360/view360.page.ts");







const routes = [
    {
        path: '',
        component: _view360_page__WEBPACK_IMPORTED_MODULE_6__["View360Page"]
    }
];
let View360PageModule = class View360PageModule {
};
View360PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_view360_page__WEBPACK_IMPORTED_MODULE_6__["View360Page"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
    })
], View360PageModule);



/***/ }),

/***/ "./src/app/pages/view360/view360.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/view360/view360.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZpZXczNjAvdmlldzM2MC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/view360/view360.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/view360/view360.page.ts ***!
  \***********************************************/
/*! exports provided: View360Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View360Page", function() { return View360Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_mysevice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mysevice.service */ "./src/app/services/mysevice.service.ts");



let View360Page = class View360Page {
    constructor(mys) {
        this.mys = mys;
    }
    ngOnInit() {
        this.view360url = this.mys.view360url;
        console.log(this.mys.view360url);
    }
};
View360Page.ctorParameters = () => [
    { type: _services_mysevice_service__WEBPACK_IMPORTED_MODULE_2__["MyseviceService"] }
];
View360Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view360',
        template: __webpack_require__(/*! raw-loader!./view360.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/view360/view360.page.html"),
        styles: [__webpack_require__(/*! ./view360.page.scss */ "./src/app/pages/view360/view360.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mysevice_service__WEBPACK_IMPORTED_MODULE_2__["MyseviceService"]])
], View360Page);



/***/ })

}]);
//# sourceMappingURL=pages-view360-view360-module-es2015.js.map