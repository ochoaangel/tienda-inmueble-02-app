(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-create-visit-create-visit-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/create-visit/create-visit.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/create-visit/create-visit.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button style=\"color: white;\" defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"color: white; text-align: center; padding-left: 40px;\"> Planificador de Visita </ion-title>\n    <ion-buttons slot=\"end\" style=\"color: white;\">\n      <ion-menu-button> </ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<!-- liCCnk jose https://drive.google.com/open?id=1LGNS90CmPV8BlQ5GIxLytDsNJ6T3cS9B -->\n<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>Ingresa Datos para la Visita</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>Día 1</ion-card-content>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h3>\n            <ion-datetime\n              displayFormat=\"DD/MM/YYYY\"\n              placeholder=\"Fecha Aquí\"\n              cancelText=\"Cancelar\"\n              doneText=\"Listo\"\n              mode=\"ios\"\n              [(ngModel)]=\"day_1\"\n              (ionChange)=\"cambioDay1()\"\n              (ionCancel)=\"cancelando()\"\n              [min]=\"minDay1\"\n              [max]=\"maxDay1\"\n              [disabled]=\"!statusDay1\"\n            ></ion-datetime>\n            <!-- <ion-datetime displayFormat=\"MM/DD/YYYY\" min=\"1994-03-14\" max=\"2012-12-09\"></ion-datetime> -->\n          </h3>\n        </ion-col>\n        <ion-col>\n          <ion-grid>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_1_a_i\"\n                (ionChange)=\"cambioT1a()\"\n                [disabled]=\"!statusTime_1_a_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_1_b_i\"\n                (ionChange)=\"cambioT1b()\"\n                [disabled]=\"!statusTime_1_b_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_1_c_i\"\n                (ionChange)=\"cambioT1c()\"\n                [disabled]=\"!statusTime_1_c_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <!-- ////////// -->\n  <ion-card>\n    <ion-card-content>Día 2</ion-card-content>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h3>\n            <ion-datetime\n              displayFormat=\"DD/MM/YYYY\"\n              placeholder=\"Fecha Aquí\"\n              cancelText=\"Cancelar\"\n              doneText=\"Listo\"\n              mode=\"ios\"\n              [(ngModel)]=\"day_2\"\n              (ionChange)=\"cambioDay2()\"\n              [min]=\"minDay2\"\n              [max]=\"maxDay2\"\n              [disabled]=\"!statusDay2\"\n            ></ion-datetime>\n          </h3>\n        </ion-col>\n        <ion-col>\n          <ion-grid>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\" \n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_2_a_i\"\n                (ionChange)=\"cambioT2a()\"\n                [disabled]=\"!statusTime_2_a_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_2_b_i\"\n                (ionChange)=\"cambioT2b()\"\n                [disabled]=\"!statusTime_2_b_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_2_c_i\"\n                (ionChange)=\"cambioT2c()\"\n                [disabled]=\"!statusTime_2_c_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <!-- //////// -->\n  <ion-card>\n    <ion-card-content>Día 3</ion-card-content>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h3>\n            <ion-datetime\n              displayFormat=\"DD/MM/YYYY\"\n              placeholder=\"Fecha Aquí\"\n              cancelText=\"Cancelar\"\n              doneText=\"Listo\"\n              mode=\"ios\"\n              [(ngModel)]=\"day_3\"\n              (ionChange)=\"cambioDay3()\"\n              [min]=\"minDay3\"\n              [max]=\"maxDay3\"\n              [disabled]=\"!statusDay3\"\n            ></ion-datetime>\n          </h3>\n        </ion-col>\n        <ion-col>\n          <ion-grid>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_3_a_i\"\n                (ionChange)=\"cambioT3a()\"\n                [disabled]=\"!statusTime_3_a_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_3_b_i\"\n                (ionChange)=\"cambioT3b()\"\n                [disabled]=\"!statusTime_3_b_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n            <ion-row>\n              <!-- <h3> -->\n              <ion-datetime\n                displayFormat=\"h:mm A\"\n                hourValues=\"9,10,11,12,13,14,15,16,17,18,19,20,21 #hour\"\n                minuteValues=\"0,15,30,45\"\n                cancelText=\"Cancelar\"\n                doneText=\"Listo\"\n                placeholder=\"Hora Aquí\"\n                mode=\"ios\"\n                [(ngModel)]=\"time_3_c_i\"\n                (ionChange)=\"cambioT3c()\"\n                [disabled]=\"!statusTime_3_c_i\"\n              ></ion-datetime>\n              <!-- </h3> -->\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-button expand=\"full\" (click)=\"GuardarCita()\">Guardar Cita</ion-button>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/create-visit/create-visit.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/create-visit/create-visit.module.ts ***!
  \***********************************************************/
/*! exports provided: CreateVisitPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateVisitPageModule", function() { return CreateVisitPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _create_visit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./create-visit.page */ "./src/app/pages/create-visit/create-visit.page.ts");







var routes = [
    {
        path: "",
        component: _create_visit_page__WEBPACK_IMPORTED_MODULE_6__["CreateVisitPage"]
    }
];
var CreateVisitPageModule = /** @class */ (function () {
    function CreateVisitPageModule() {
    }
    CreateVisitPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_create_visit_page__WEBPACK_IMPORTED_MODULE_6__["CreateVisitPage"]]
        })
    ], CreateVisitPageModule);
    return CreateVisitPageModule;
}());



/***/ }),

/***/ "./src/app/pages/create-visit/create-visit.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/create-visit/create-visit.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NyZWF0ZS12aXNpdC9jcmVhdGUtdmlzaXQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/create-visit/create-visit.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/create-visit/create-visit.page.ts ***!
  \*********************************************************/
/*! exports provided: CreateVisitPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateVisitPage", function() { return CreateVisitPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



// nuevos


var CreateVisitPage = /** @class */ (function () {
    function CreateVisitPage(router, alertCtrl) {
        // this.customPickerOptions = {
        //   buttons: [{
        //     text: 'Save',
        //     handler: () => {console.log('Clicked Save!'); return true;}
        //   }, {
        //     text: 'Log',
        //     handler: () => {
        //       console.log('Clicked Log. Do not Dismiss.');
        //       return false;
        //     }
        //   }]
        // }
        // }
        this.router = router;
        this.alertCtrl = alertCtrl;
        // tslint:disable:variable-name
        this.secuencia = 1;
        this.incompleto = true;
        this.statusDay1 = true;
        this.statusDay2 = false;
        this.statusDay3 = false;
        this.statusTime_1_a_i = false;
        this.statusTime_1_b_i = false;
        this.statusTime_1_c_i = false;
        this.statusTime_2_a_i = false;
        this.statusTime_2_b_i = false;
        this.statusTime_2_c_i = false;
        this.statusTime_3_a_i = false;
        this.statusTime_3_b_i = false;
        this.statusTime_3_c_i = false;
    }
    CreateVisitPage.prototype.ngOnInit = function () {
        var nowX = moment__WEBPACK_IMPORTED_MODULE_2__();
        var minDay1Complete = nowX.clone().add(2, 'days');
        var maxDay1Complete = minDay1Complete.clone().add(30, 'days');
        this.minDay1 = minDay1Complete.format('YYYY-MM-DD');
        this.maxDay1 = maxDay1Complete.format('YYYY-MM-DD');
        // this.maxDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
        console.log('this.minDay1 ', this.minDay1);
        console.log('this.maxDay1 ', this.maxDay1);
    };
    CreateVisitPage.prototype.GuardarCita = function () {
        this.verificarFechas();
    }; // fin GuardarCita
    CreateVisitPage.prototype.verificarFechas = function () {
        console.log(this.time_1_a_i);
        var nowX = moment__WEBPACK_IMPORTED_MODULE_2__();
        console.log(nowX.format());
        var tomorrowX = nowX.clone().add(1, 'days');
        this.minDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
        this.minDay2 = nowX.clone().add(2, 'days').format('YYYY-MM-DD');
        this.minDay3 = nowX.clone().add(3, 'days').format('YYYY-MM-DD');
        // comprobando el primer dia
        if (tomorrowX.isBefore(moment__WEBPACK_IMPORTED_MODULE_2__(this.day_1))) {
            console.log('El primer dia VALIDO es luego de lo permitido');
            // comprobando el 2do dia
            if (moment__WEBPACK_IMPORTED_MODULE_2__(this.day_1).add(1, 'days').isBefore(moment__WEBPACK_IMPORTED_MODULE_2__(this.day_2))) {
                console.log('El Segundo dia VALIDO es luego de lo permitido');
                // comprobando el 3er dia
                if (moment__WEBPACK_IMPORTED_MODULE_2__(this.day_2).add(1, 'days').isBefore(moment__WEBPACK_IMPORTED_MODULE_2__(this.day_3))) {
                    console.log('El Tercer dia VALIDO es luego de lo permitido');
                    console.log('Todo Cumple');
                }
                else {
                    console.log('El Tercer dia es Antes de lo permitido');
                }
            }
            else {
                console.log('El Segundo dia es Antes de lo permitido');
            }
        }
        else {
            console.log('El primer dia es Antes de lo permitido');
        }
    }; // fin verificarFechas
    CreateVisitPage.prototype.verificarHorasDia1 = function () {
        console.log(this.time_1_a_i);
        // const nowX = moment();
        // console.log(nowX.format());
        // const tomorrowX = nowX.clone().add(1, 'days');
        // this.minDay1 = nowX.clone().add(1, 'days').format('YYYY-MM-DD');
        // this.minDay2 = nowX.clone().add(2, 'days').format('YYYY-MM-DD');
        // this.minDay3 = nowX.clone().add(3, 'days').format('YYYY-MM-DD');
        // // comprobando la 2da Hora
        // if (moment(this.day_1).isBefore(moment(this.day_2))) {
        //   console.log('El Segundo dia VALIDO es luego de lo permitido');
        //   // comprobando el 3er dia
        //   if (moment(this.day_2).isBefore(moment(this.day_3))) {
        //     console.log('El Tercer dia VALIDO es luego de lo permitido');
        //     console.log('Todo Cumple');
        //   } else {
        //     console.log('El Tercer dia es Antes de lo permitido');
        //   }
        // } else {
        //   console.log('El Segundo dia es Antes de lo permitido');
        // }
        if (moment__WEBPACK_IMPORTED_MODULE_2__(this.time_1_a_i).isBefore(moment__WEBPACK_IMPORTED_MODULE_2__(this.time_1_b_i))) {
            if (moment__WEBPACK_IMPORTED_MODULE_2__(this.time_1_b_i).isBefore(moment__WEBPACK_IMPORTED_MODULE_2__(this.time_1_c_i))) {
            }
        }
        else {
            console.log('Error, la hora A es menor que la B');
        }
    }; // fin verificarFechas
    // para evitar error al deshabilitar componentes
    CreateVisitPage.prototype.cambioDay1 = function () {
        var _this = this;
        this.cambioDey1();
        setTimeout(function () { _this.cambioDey1(); }, 250);
    };
    CreateVisitPage.prototype.cambioDay2 = function () {
        var _this = this;
        // this.cambioDey2();
        // setTimeout(() => { this.cambioDey2(); }, 250);
        setTimeout(function () { _this.cambioDey2(); }, 500);
    };
    CreateVisitPage.prototype.cambioDay3 = function () {
        var _this = this;
        this.cambioDey3();
        setTimeout(function () { _this.cambioDey3(); }, 250);
        setTimeout(function () { _this.cambioDey3(); }, 500);
    };
    //  funciones principales
    CreateVisitPage.prototype.cambioDey1 = function () {
        // this.day_1 = '';
        this.time_1_a_i = '';
        this.time_1_b_i = '';
        this.time_1_c_i = '';
        this.day_2 = '';
        this.time_2_a_i = '';
        this.time_2_b_i = '';
        this.time_2_c_i = '';
        this.day_3 = '';
        this.time_3_a_i = '';
        this.time_3_b_i = '';
        this.time_3_c_i = '';
        this.statusDay1 = true;
        this.statusDay2 = false;
        this.statusDay3 = false;
        this.statusTime_1_a_i = true;
        this.statusTime_1_b_i = false;
        this.statusTime_1_c_i = false;
        this.statusTime_2_a_i = false;
        this.statusTime_2_b_i = false;
        this.statusTime_2_c_i = false;
        this.statusTime_3_a_i = false;
        this.statusTime_3_b_i = false;
        this.statusTime_3_c_i = false;
        console.log('Cambio el Dia1');
    };
    CreateVisitPage.prototype.cambioDey2 = function () {
        // this.day_1 = '';
        // this.time_1_a_i = '';
        // this.time_1_b_i = '';
        // this.time_1_c_i = '';
        // this.day_2 = '';
        this.time_2_a_i = '';
        this.time_2_b_i = '';
        this.time_2_c_i = '';
        this.day_3 = '';
        this.time_3_a_i = '';
        this.time_3_b_i = '';
        this.time_3_c_i = '';
        this.statusDay1 = true;
        this.statusDay2 = true;
        this.statusDay3 = false;
        this.statusTime_1_a_i = true;
        this.statusTime_1_b_i = true;
        this.statusTime_1_c_i = true;
        this.statusTime_2_a_i = true;
        this.statusTime_2_b_i = false;
        this.statusTime_2_c_i = false;
        this.statusTime_3_a_i = false;
        this.statusTime_3_b_i = false;
        this.statusTime_3_c_i = false;
        console.log('Cambio el Dia2');
    };
    CreateVisitPage.prototype.cambioDey3 = function () {
        // this.day_1 = '';
        // this.time_1_a_i = '';
        // this.time_1_b_i = '';
        // this.time_1_c_i = '';
        // this.day_2 = '';
        // this.time_2_a_i = '';
        // this.time_2_b_i = '';
        // this.time_2_c_i = '';
        // this.day_3 = '';
        this.time_3_a_i = '';
        this.time_3_b_i = '';
        this.time_3_c_i = '';
        this.statusDay1 = true;
        this.statusDay2 = true;
        this.statusDay3 = true;
        this.statusTime_1_a_i = true;
        this.statusTime_1_b_i = true;
        this.statusTime_1_c_i = true;
        this.statusTime_2_a_i = true;
        this.statusTime_2_b_i = true;
        this.statusTime_2_c_i = true;
        this.statusTime_3_a_i = true;
        this.statusTime_3_b_i = false;
        this.statusTime_3_c_i = false;
        console.log('Cambio el Dia3');
    };
    CreateVisitPage.prototype.cambioT1a = function () { this.statusTime_1_b_i = true; };
    CreateVisitPage.prototype.cambioT1b = function () { this.statusTime_1_c_i = true; };
    CreateVisitPage.prototype.cambioT1c = function () { this.statusDay2 = true; };
    CreateVisitPage.prototype.cambioT2a = function () { this.statusTime_2_b_i = true; };
    CreateVisitPage.prototype.cambioT2b = function () { this.statusTime_2_c_i = true; };
    CreateVisitPage.prototype.cambioT2c = function () { this.statusDay3 = true; };
    CreateVisitPage.prototype.cambioT3a = function () { this.statusTime_3_b_i = true; };
    CreateVisitPage.prototype.cambioT3b = function () { this.statusTime_3_c_i = true; };
    CreateVisitPage.prototype.cambioT3c = function () { console.log('exitooooo Terminó todo'); };
    CreateVisitPage.prototype.cancelando = function () {
        console.log('cancelandoooooooooooooo');
    };
    CreateVisitPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
    ]; };
    CreateVisitPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-create-visit',
            template: __webpack_require__(/*! raw-loader!./create-visit.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/create-visit/create-visit.page.html"),
            styles: [__webpack_require__(/*! ./create-visit.page.scss */ "./src/app/pages/create-visit/create-visit.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
    ], CreateVisitPage);
    return CreateVisitPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-create-visit-create-visit-module-es5.js.map